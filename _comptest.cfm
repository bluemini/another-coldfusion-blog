<cfset string1 = "Architect: Foster & Partners Capital Cost: ##200_000_000 Structural, geotechnical and fire engineering design plus traffic studies and project planning for a new 62-storey landmark">
<cfset string2 = "Architect: Sir Norman Foster & Partners Capital Cost: ##200_000_000 Structural, geotechnical , public health and complete fire engineering design and project planning for a new 62-storey landmark">

<cfif Compare(string1, string2)>

	<cfset List1 = REReplace(Replace(string1, " ", ",", "ALL"), "(,)+", ",", "ALL")>
	<cfset List2 = REReplace(Replace(string2, " ", ",", "ALL"), "(,)+", ",", "ALL")>
	
	<cfif ListLen(List1) NEQ ListLen(List2)>
		<cfif ListLen(List1) GT ListLen(List2)>
			<cfloop index="Diff" from="1" to="#Evaluate(ListLen(List1)-ListLen(List2))#" step="1">
				<cfset List2 = ListAppend(List2, "|blank|")>
			</cfloop>
		<cfelse>
			<cfloop index="Diff" from="1" to="#Evaluate(ListLen(List2)-ListLen(List1))#" step="1">
				<cfset List1 = ListAppend(List1, "|blank|")>
			</cfloop>
		</cfif>		
	</cfif>
	
	<table border="1" cellpadding="5">
		<tr>
			<td>Pos</td>
			<td>String1</td>
			<td>string2</td>
			<td>old in new</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>	
	<cfloop index="Count" from="1" to="#ListLen(List1)#" step="1">

	<cfoutput>
		<tr>
			<td>#count#</td>
			<td>#ListGetAt(List1, Count)#</td>
			<td>#ListGetAt(List2, Count)#</td>
			<td>
				<!--- If the second string is the same as the first string record the count number --->
				<cfif NOT Compare(ListGetAt(List1, Count), ListGetAt(List2, Count))>
					#Count#
				<cfelse>
				<!--- Otherwise, loop through the second string, starting from (count + 1) until a match is found.
				Record a zero for no match and Count for the match. Stop at the first match --->
					<cfset Count_2 = Evaluate(Count + 1)>
					<cfloop index="SubCount" from="#Count_2#" to="#ListLen(List1)#" step="1">
						<cfif NOT Compare(ListGetAt(List1, Count), ListGetAt(List2, SubCount))>
							#Count#
						<cfelse>
							#Count#
						</cfif>
						<cfbreak>
					</cfloop>
				</cfif>				
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</cfoutput>

	</cfloop>
	</table>

<cfelse>
	<cfset string3 = "They are the same.">
</cfif>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Highlight Changes</title>
</head>

<body>

<cfoutput>

</cfoutput>

</body>
</html>
