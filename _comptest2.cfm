<!---
	An algorithm for comparing two text strings and highlighting where they have changed
	
	Sometime around 2005 (I'm guessing) - Kevin Franklin (http://www.hairyegg.com/)
	20 Jan 2011 - Nick Harvey - Added in this credit and output the two original strings to better asses the routine's analysis
--->
<cfset string1 = "Architect: Foster & Partners Capital Cost: ##200_000_000 Structural, geotechnical and fire engineering design plus traffic studies and project planning for a new 62-storey landmark">
<cfset string2 = " Capital Cost: ##200_000_000 Structural, geotechnical , public health and complete fire engineering design and project planning for a new 62-storey landmark">

<cfif Compare(string1, string2)>
	<!--- Original string as list --->
	<cfset List1 = REReplace(Replace(Trim(string1), " ", ",", "ALL"), "(,)+", ",", "ALL")>
	<!--- Modified string as list --->
	<cfset List2 = REReplace(Replace(Trim(string2), " ", ",", "ALL"), "(,)+", ",", "ALL")>
	
	<cfif ListLen(List1) NEQ ListLen(List2)>
		<cfif ListLen(List1) GT ListLen(List2)>
			<cfloop index="Diff" from="1" to="#Evaluate(ListLen(List1)-ListLen(List2))#" step="1">
				<cfset List2 = ListAppend(List2, "|blank|")>
			</cfloop>
		<cfelse>
			<cfloop index="Diff" from="1" to="#Evaluate(ListLen(List2)-ListLen(List1))#" step="1">
				<cfset List1 = ListAppend(List1, "|blank|")>
			</cfloop>
		</cfif>		
	</cfif>

	<cfset List3 = List1>
	<cfset List4 = "">
	<cfset LastUsed = 0>
	
	<table border="1" cellpadding="5">
		<tr>
			<td>Pos</td>
			<td>String1</td>
			<td>string2</td>
			<td>Position in String</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>	
	<cfloop index="Count" from="1" to="#ListLen(List1)#" step="1">

	<cfoutput>
		<tr>
			<td>#count#</td>
			<td>#ListGetAt(List1, Count)#</td>
			<td>#ListGetAt(List2, Count)#</td>
			<td>
				<cfset Position = ListFind(List3, ListGetAt(List2, Count))>
				#ListFind(List3, ListGetAt(List2, Count))#
				
				<cfif ListFind(List3, ListGetAt(List2, Count)) GT 0>
					<cfset List3 = ListSetAt(List3, ListFind(List3, ListGetAt(List2, Count)), "|used|")>
				</cfif>	
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</cfoutput>	
	
	<cfif ListGetAt(List2, Count) IS "|blank|"><cfbreak></cfif>
	
	<cfif Position IS 0>
		<cfset List4 = ListAppend(List4, "+#Count#")>
	<cfelseif NOT Position IS Evaluate(LastUsed + 1)>
		<cfloop index="Deleted" from="#Evaluate(LastUsed + 1)#" to="#Evaluate(Position - 1)#" step="1">
			<cfset List4 = ListAppend(List4, "-#Deleted#")>
		</cfloop>
			<cfset List4 = ListAppend(List4, Position)>
	<cfelse>
		<cfset List4 = ListAppend(List4, Position)>
	</cfif>
	
	<cfif Position IS NOT 0>
		<cfset LastUsed = Position>
	</cfif>
	
	</cfloop>
	</table>

<cfelse>
	<cfset string4 = "No edits have been made.">
</cfif>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Highlight Changes</title>
</head>

<body>

<cfoutput>
#List4#<br><br><br>

<h3>Comparing</h3>
<p><strong>String 1:</strong>#string1#</p>
<p><strong>String 2:</strong>#string2#</p>

<cfloop index="Word" list="#List4#" delimiters=",">
	<cfif Word CONTAINS '+'>
		<font color="##FF0000">#ListGetAt(List2, RemoveChars(Word, 1, 1))#</font>
	<cfelseif Word CONTAINS '-'>
		<font color="##C0C0C0">#ListGetAt(List1, RemoveChars(Word, 1, 1))#</font>
	<cfelse>
		#ListGetAt(List1, Word)#
	</cfif>
</cfloop>

</cfoutput>

</body>
</html>
