<!--- create the obj_post object, and then validate and save the form content --->

<cfparam name="form.blogid" type="numeric">
<cfset objBlog = CreateObject("component", "components.obj_blog")>
<cfset qryBlog = objBlog.getBlog(form.blogid)>

<cfif NOT StructKeyExists(cookie, "userid")>
	<cflocation url="/auth/index.cfm?location=#URLEncodedFormat(cgi.PATH_INFO&"?"&cgi.QUERY_STRING)#" addtoken="yes">
<cfelseif cookie.userid IS NOT qryBlog.blog_user_id>
	<cfthrow message="you are not authorised to post new items to this blog">
</cfif>

<!--- if the user hit cancel, then cancel --->
<cfif StructKeyExists(form, "cancel")>
	<cflocation url="/?blogid=#form.blogid#" addtoken="false">
</cfif>


<!--- if a post id is supplied, then update the post, otherwise, create a new one --->
<cfset objPost = CreateObject("component", "components.obj_post")>
<cfset newId = objPost.saveItem(form)>

<!--- index the new item --->
<cfset cleanBody = REReplace(form.postbody, "<[^>]*>", "", "ALL")>
<cfset cleanTitle = REReplace(form.posttitle, "<[^>]*>", "", "ALL")>
<cfindex action="update" collection="blogitems" type="custom" urlpath="/?blogid=#form.blogid#&post=#newid#" key="#newid#" title="#cleanTitle#" body="#cleanBody#" custom1="#form.blogid#">

<cfif newId GT 0>
	<cfif StructKeyExists(form, "save")>
    	<cflocation url="/?blogid=#form.blogid#&post=#newid#" addtoken="false">
	<cfelse>
    	<cflocation url="/?blogid=#form.blogid#" addtoken="false">
    </cfif>
</cfif>

<cfdump var="#form#">