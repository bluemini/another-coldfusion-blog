CREATE TABLE `nickblog`.`t_wikis` (
  `wiki_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wiki_name` varchar(45) NOT NULL COMMENT 'The name that displays in the title bar and the blog header',
  `wiki_owner_name` varchar(45) NOT NULL COMMENT 'The name to display as the owner',
  `wiki_about_text` varchar(1000) NOT NULL COMMENT 'The summary of the blog',
  `wiki_user_id` varchar(50) NOT NULL COMMENT 'An id associated with the user',
  `wiki_user_division` varchar(45) NOT NULL COMMENT 'An id that relates the user to a company division',
  PRIMARY KEY (`wiki_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='A list of all blogs'$$



CREATE TABLE `nickblog`.`t_pages` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_title` varchar(100) NOT NULL,
  `page_slug` varchar(100) NOT NULL,
  `page_date` datetime NOT NULL,
  `page_comment_count` int(10) unsigned NOT NULL,
  `page_date_modified` datetime NOT NULL,
  `page_content` longtext NOT NULL,
  `page_valid` smallint(5) unsigned NOT NULL DEFAULT '1',
  `page_wiki_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'the blog id to which this item relates',
  `page_version` int(11) NOT NULL DEFAULT '1',
  `page_editor_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`,`page_version`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1$$