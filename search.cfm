<cfsilent>

	<cfinclude template="./rhino_core/rhino.cfm">

	<!--- check for and set some defaults --->
    <cfparam name="url.post" default="0">
    
    <cfif NOT StructKeyExists(url, "blogid") OR NOT IsNumeric(url.blogid)>
    	<cflocation url="/?blogid=1" addtoken="false">
    </cfif>
    
    <!--- create any objects that we need --->
	<cfset objBlog = request.data.getObject("blog.obj_blog")>
    
    <!--- fetch high level blog information --->
    <cfset qryBlog = objBlog.getBlog(url.blogid)>
    
	<!--- do the search --->
	<cfparam name="url.search">
	<cfset categories = "">
	<cfsearch name="results" collection="blogitems" criteria="#url.search#" category="#categories#" maxrows="10">
    
</cfsilent><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><cfinclude template="/applications/acfblog/pagelets/mainblog/dsp_head.cfm"></head>

<body>
<div id="content">

    <div class="top-heading">
        <h1><cfoutput><a href="/?blogid=#qryBlog.blog_id#" style="color:inherit;text-decoration:none">#qryBlog.blog_name#</a></cfoutput></h1>
    </div>
    
    <div id="maincontent">
    
        <div id="rightnav" class="col3 right">
			<cfinclude template="/applications/acfblog/pagelets/pods/dsp_search.cfm">
            <!---
			<cfinclude template="pagelets/pods/dsp_labels.cfm">
            <cfinclude template="pagelets/pods/dsp_aboutme.cfm">
            <cfinclude template="pagelets/pods/dsp_recentposts.cfm">
			--->
            
            <cfinclude template="/applications/acfblog/pagelets/pods/dsp_admin.cfm">
        </div><!--- end rightnav --->
    
        <div id="body" class="col6 left">
        	<h2 class="top-align post-title">Search Results</h2>
			<cfoutput query="results"><p>
				<a href="/?blogid=#custom1#&post=#key#">#title#</a><br>
			    #summary#</p></cfoutput>
            <cfif results.recordCount EQ 0><p>Sorry, your search for <strong><cfoutput>#HTMLEditFormat(url.search)#</cfoutput></strong> didn't return any results.</p></cfif>
        </div><!--- /id=body --->
        
        <div id="footer"><cfinclude template="/applications/acfblog/pagelets/mainblog/dsp_footer.cfm"></div>
    </div><!--- end maincontent --->
</div><!--- end content --->
</body>
</html>