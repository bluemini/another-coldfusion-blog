<!--- create the obj_post object, and then validate and save the form content --->

<cfparam name="form.blogid">
<cfset objBlog = request.data.getObject("blog.obj_blog")>
<cfset qryBlog = objBlog.getBlog(form.blogid)>

<cfif NOT StructKeyExists(cookie, "acfbid")>
	<cflocation url="/auth/index.cfm?location=#URLEncodedFormat(cgi.PATH_INFO&"?"&cgi.QUERY_STRING)#" addtoken="yes">
<cfelseif cookie.acfbid IS NOT qryBlog.blog_user_id>
	<cfthrow message="you are not authorised to post new items to this blog">
</cfif>


<!--- if the user hit cancel, then cancel --->
<cfif NOT StructKeyExists(form, "cancel")>
	
	
	<!--- if a post id is supplied, then update the post, otherwise, create a new one --->
	<cfset objPost = request.data.getObject("blog.obj_post")>
	<cfset newId = objPost.saveItem(form, request.ses)>
	
	
	<!--- index the new item --->
	<cfset cleanBody = REReplace(form.postbody, "<[^>]*>", "", "ALL")>
	<cfset cleanTitle = REReplace(form.posttitle, "<[^>]*>", "", "ALL")>
	<cfindex action="update" collection="blogitems" type="custom" urlpath="/?blogid=#form.blogid#&post=#newid#" key="#newid#" title="#cleanTitle#" body="#cleanBody#" custom1="#form.blogid#">
	
	<cfif newId GT 0>
		<cfif StructKeyExists(form, "save")>
			<cfif request.ses>
				<cflocation url="/blog/#qryBlog.blog_name_user#/#newid#" addtoken="false">
			<cfelse>
				<cflocation url="/?blogid=#form.blogid#&post=#newid#" addtoken="false">
			</cfif>
		<cfelse>
			<cfif request.ses>
				<cflocation url="/blog/#qryBlog.blog_name_user#" addtoken="false">
			<cfelse>
				<cflocation url="/?blogid=#form.blogid#" addtoken="false">
			</cfif>
		</cfif>
	</cfif>

<cfelse>
	<cfif request.ses>
		<cflocation url="/blog/#qryBlog.blog_name_user#" addtoken="false">
	<cfelse>
		<cflocation url="/?blogid=#form.blogid#" addtoken="false">
	</cfif>
</cfif>

<cfthrow message="Ooops, reached code that should be unreachable in the POST update controller">