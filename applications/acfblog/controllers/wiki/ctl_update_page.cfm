<!--- create the obj_post object, and then validate and save the form content --->

<cfparam name="attributes.wikiid" type="numeric">
<cfset objWiki = request.data.getObject("wiki.obj_wiki")>
<cfset qryWiki = objWiki.getWiki(form.wikiid)>

<cfif NOT StructKeyExists(cookie, "acfbid")>
	<cflocation url="/auth/index.cfm?location=#URLEncodedFormat(cgi.PATH_INFO&"?"&cgi.QUERY_STRING)#" addtoken="yes">
</cfif>

<cfset canEdit = objWiki.isEditor(cookie.acfbid)>
<cfif NOT canEdit>
	<cfthrow message="you are not authorised to post new items to this blog">
</cfif>

<!--- if the user hit cancel, then cancel --->
<cfif StructKeyExists(attributes, "cancel")>
	<cfparam name="attributes.page" type="numeric" default="1">
	<cflocation url="/?layout=wiki&wikiid=#form.wikiid#&page=#form.page#" addtoken="false">
</cfif>


<!--- if a post id is supplied, then update the post, otherwise, create a new one --->
<cfset objPage = request.data.getObject("wiki.obj_page")>
<cfset newId = objPage.savePage(attributes, "slug")>

<!--- index the new item --->
<cfset cleanBody = REReplace(form.pagebody, "<[^>]*>", "", "ALL")>
<cfset cleanTitle = REReplace(form.pagetitle, "<[^>]*>", "", "ALL")>
<cfindex action="update" collection="wikiitems" type="custom" urlpath="/?wikiid=#form.wikiid#&page=#newid#" key="#newid#" title="#cleanTitle#" body="#cleanBody#" custom1="#form.wikiid#">

<cfif newId GT 0>
	<cfif StructKeyExists(attributes, "save")>
    	<cflocation url="/wiki/#qryWiki.wiki_name_user#/#newid#" addtoken="false">
	<cfelse>
    	<cflocation url="/wiki/#qryWiki.wiki_name_user#" addtoken="false">
    </cfif>
</cfif>

<cfdump var="#form#"><cfdump var="#attributes#">