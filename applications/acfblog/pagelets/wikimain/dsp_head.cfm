<cfsilent>

	<!--- check for and set some defaults --->
    <cfset request.page.title = "Wiki">

    <cfparam name="attributes.wikiid" default="1">
    <cfparam name="attributes.page" default="2">

	<cfset request.objWiki = request.data.getObject("wiki.obj_wiki")>
    <cfset request.objPage = request.data.getObject("wiki.obj_page")>

    <!--- fetch high level wiki information --->
    <cfset request.qryWiki = request.objWiki.getWiki(attributes.wikiid)>

    <cfif request.qryWiki.recordCount EQ 0><cfthrow message="wiki cannot be found"></cfif>

    <cfset request.qryPage = request.objPage.getPage(wikiid=request.qryWiki.wiki_id,
										 pageid=attributes.page,
										 includeDraft=true)>
	<cfset request.page_text = request.qryPage.page_content>

	<cfset request.canEdit = request.objWiki.isEditor(cookie.acfbid)>
    <cfset request.canView = request.objWiki.isViewer(cookie.acfbid)>
	<cfif NOT request.canEdit>
		<cfthrow message="you are not authorised to post new items to this blog">
    </cfif>

</cfsilent><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<cfif StructKeyExists(request, "qryWiki") AND StructKeyExists(request, "qryPage")><cfoutput><title>#request.qryPage.page_title# - #request.qryWiki.wiki_owner_name# - #request.qryWiki.wiki_name#</title>
<!---
<link href="/rss/rss.cfm?blogid=#request.qryBlog.blog_id#" rel="alternate" type="application/rss+xml" title="My Blog - "> ---></cfoutput>
<cfelseif StructKeyExists(request, "wikipage")><title>#wikipage.wiki_title# - #wikipage.wiki_user_name# - Wiki</title></cfif>
<link href="/styles/main.css" rel="stylesheet" type="text/css" />
<link href="/styles/syntax.css" rel="stylesheet" type="text/css" />
<!-- <link href="/plugins/syntaxhighlighter3/styles/shThemeDefault.css" rel="stylesheet" type="text/css" /> -->
