<cfsilent>

    <cfparam name="attributes.wikiid" type="numeric" default="1">
    <cfparam name="attributes.page" default="1" type="numeric">

	<cfset objWiki = request.data.getObject("wiki.obj_wiki")>
    <cfset objPage = request.data.getObject("wiki.obj_page")>

    <!--- fetch high level wiki information --->
    <cfset qryWiki = objWiki.getWiki(url.wikiid)>

    <cfif qryWiki.recordCount EQ 0><cfthrow message="wiki cannot be found"></cfif>

    <cfset qryPage = objPage.getPageById(wikiid=attributes.wikiid,
										 pageid=attributes.page,
										 includeDraft=true)>
	<cfset page_text = qryPage.page_content>

	<cfset canEdit = objWiki.isEditor(cookie.acfbid)>
    <cfset canView = objWiki.isViewer(cookie.acfbid)>
	<cfif NOT canEdit>
		<cfthrow message="you are not authorised to post new items to this blog">
    </cfif>

</cfsilent><cfoutput><div id="post"><form action="index.cfm" method="post">
                <h2 class="top-align post-title">Edit Page</h2>
                <input type="text" name="pagetitle" class="big-form" value="#qryPage.page_title#">
                <textarea name="pagebody" id="pagebody" rows="20" wrap="virtual" style="width: 100%">#HTMLEditFormat(qryPage.page_content)#</textarea>
                <!--- removed the spell check, it was before 'undo', ,'Scayt' --->
                <div class="button-bar">
	                <input type="hidden" name="layout" value="do_updatewikipage">
                	<input type="hidden" name="wikiid" value="#url.wikiid#">
                    <cfif qryPage.page_id IS NOT ""><input type="hidden" name="page" value="#qryPage.page_id#"></cfif>
                    <cfif qryPage.page_valid NEQ 1><input type="submit" name="draft" value="Save Draft"></cfif>
	                <input type="submit" name="save" value="Save Page">
	                <input type="submit" name="cancel" value="Cancel" class="cancel">
                </div>
            </form></div></cfoutput><!--- end post --->