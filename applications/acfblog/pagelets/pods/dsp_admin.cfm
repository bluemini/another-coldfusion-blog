<cfif StructKeyExists(cookie, "acfbid") AND StructKeyExists(request, "qryBlog") AND cookie.acfbid IS request.qryBlog.blog_user_id><div id="admin" class="poddy"><cfoutput>
    <h2>Admin</h2>
    <ul>
		<li><a href="/blog/#request.qryBlog.blog_name_user#/new">new post</a></li>
		<cfif StructKeyExists(request, "qryPost")><li><a href="/blog/#request.qryBlog.blog_id#/edit/#request.qryPost.post_id#">edit post</a></li></cfif>
	</ul>
	<!--- get any draft posts so that we can edit/publish them --->
	<cfset objPostFactory = request.data.getObject("blog.obj_post_factory")>
	<cfset drafts = objPostFactory.getDrafts(request.qryBlog.blog_id)>
	<cfif drafts.RecordCount GT 0>
	<h3>Draft Posts</h3>
	<ul>
		<cfoutput query="drafts"><li><a href="/blog/#request.qryBlog.blog_id#/edit/#drafts.post_id#">#Left(post_title, 25)#</a></li>
		</cfoutput>
	</ul>
	</cfif>
</div></cfoutput><cfelse><!--- <cfoutput>#request.qryBlog.blog_user_id#</cfoutput> ---></cfif>