<cfsilent>
    <!--- fetch information about the post --->
    <cfparam name="url.post" default="">
    <cfset qryPost = request.objPost.getPost(request.qryBlog.blog_id, url.post)>
    <cfif StructKeyExists(cookie, "userid") AND cookie.userid IS request.qryBlog.blog_user_id>
	    <cfset qryRecent = request.objPost.getLatestPosts(blogid=request.qryBlog.blog_id, includeDraft=true)>
    <cfelse>
	    <cfset qryRecent = request.objPost.getLatestPosts(request.qryBlog.blog_id)>
    </cfif>
</cfsilent>			<div id="posts" class="poddy">
				<h2>Recent Posts</h2>
				<ul>
				<cfoutput query="qryRecent">	<li><cfif post_valid EQ 1><a href="/blog/#request.qryBlog.blog_name_user#/#post_slug#">#post_title#</a><cfelse><a href="/?layout=blog.edit&amp;blogid=#request.qryBlog.blog_id#&amp;post=#post_id#">#post_title#</a> (draft)</cfif></li>
				</cfoutput></ul>
                <cfif qryRecent.recordCount EQ 0><ul><li>No other posts</li></ul></cfif>
            </div>