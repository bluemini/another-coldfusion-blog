<cfsilent>

	<!--- check for and set some defaults --->
    <cfparam name="attributes.blogid" default="1">
    <cfparam name="attributes.post" default="0">
    
    <!--- <cfif NOT StructKeyExists(url, "blogid")>
    	<cflocation url="/blog/NickHarvey" addtoken="false">
    </cfif> --->
    
    <!--- create any objects that we need --->
	<cfset request.objBlog = request.data.getObject("blog.obj_blog")>
	<cfset request.objPost = request.data.getObject("blog.obj_post")>
    
    <!--- fetch high level blog information --->
    <cfset request.qryBlog = request.objBlog.getBlog(attributes.blogid)>

    <!--- if the query has no records, then the blog couldn't be found --->
    <cfif request.qryBlog.recordCount EQ 0><cfthrow message="No blog exists with that ID"></cfif>
    
    <!--- fetch information about the post --->
    <cfset request.qryPost = request.objPost.getPost(request.qryBlog.blog_id, attributes.post)>
    <cfif StructKeyExists(cookie, "userid") AND cookie.userid IS request.qryBlog.blog_user_id>
	    <cfset request.qryRecent = request.objPost.getLatestPosts(blogid=request.qryBlog.blog_id, includeDraft=true)>
    <cfelse>
	    <cfset request.qryRecent = request.objPost.getLatestPosts(request.qryBlog.blog_id)>
    </cfif>
    
    <!--- if there is no items to report, then we need to setup a query that handles it --->
    <cfif request.qryPost.recordCount EQ 0>
        <cfset QueryAddRow(request.qryPost, 1)>
        <cfset QuerySetCell(request.qryPost, "post_title", "no items have been posted")>
    </cfif>

</cfsilent><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<cfif StructKeyExists(request, "qryBlog") AND StructKeyExists(request, "qryPost")><cfoutput><title>#request.qryPost.post_title# - #request.qryBlog.blog_user_name# - #request.qryBlog.blog_name#</title>
<link href="/rss/rss.cfm?blogid=#request.qryBlog.blog_id#" rel="alternate" type="application/rss+xml" title="My Blog - "></cfoutput>
<cfelseif StructKeyExistS(variables, "wikipage")><title>#wikipage.wiki_title# - #wikipage.wiki_user_name# - Wiki</title></cfif>
<link href="/styles/main.css" rel="stylesheet" type="text/css" />
<link href="/styles/syntax.css" rel="stylesheet" type="text/css" />
<!-- <link href="/plugins/syntaxhighlighter3/styles/shThemeDefault.css" rel="stylesheet" type="text/css" /> -->
