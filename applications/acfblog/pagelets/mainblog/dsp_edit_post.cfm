<cfsilent>

    <cfparam name="attributes.blogid">
    <cfparam name="attributes.post" default="0" type="numeric">
    
	<cfset request.objBlog = request.data.getObject("blog.obj_blog")>
    <cfset request.objPost = request.data.getObject("blog.obj_post")>

    <!--- fetch high level blog information --->
    <cfset qryBlog = request.objBlog.getBlog(attributes.blogid)>
	<cfset blogId  = qryBlog.blog_id>
    
    <cfset qryPost = request.objPost.getPostById(blogid=blogid,
										 postid=attributes.post,
										 includeDraft=true)>
	<cfset post_text = qryPost.post_content>
    
    <cfif NOT StructKeyExists(cookie, "acfbid") OR cookie.acfbid IS NOT qryBlog.blog_user_id>
    	<cflocation url="forbidden.cfm" addtoken="false">
    </cfif>
    
</cfsilent><cfoutput><div id="post"><form action="/index.cfm" method="post" enctype="multipart/form-data">
                <h2 class="top-align post-title">Edit Post</h2>
                <input type="text" name="posttitle" class="big-form" value="#qryPost.post_title#">
                <textarea name="postbody" id="postbody" rows="20" wrap="virtual" style="width:100%">#HTMLEditFormat(qryPost.post_content)#</textarea>
                <!--- removed the spell check, it was before 'undo', ,'Scayt' --->
                <script type="text/javascript">
				CKEDITOR.replace('postbody', {
					toolbar :
			           [
				           ['PasteFromWord','-','Undo','Redo','-','Image','SpecialChar','-','Bold','Italic','-','NumberedList','BulletedList','Outdent','Indent','-',
					        'Link','Unlink','-','Source','-','Maximize','-','About']
			    		],
			    	resize_maxWidth : '100%',
        			filebrowserBrowseUrl : "/pagelets/editor/browse.cfm",
			        filebrowserImageBrowseUrl : "/pagelets/editor/browse.cfm?type=images",
			        filebrowserUploadUrl : "/pagelets/editor/upload.cfm",
			        filebrowserImageUploadUrl : "/pagelets/editor/upload.cfm?type=images"
			    });
				</script>
                <div class="button-bar">
                	<input type="hidden" name="blogid" value="#url.blogid#">
                    <input type="hidden" name="layout" value="do_update_post">
                    <cfif qryPost.post_id IS NOT ""><input type="hidden" name="post" value="#qryPost.post_id#"></cfif>
                    <cfif qryPost.post_valid NEQ 1><input type="submit" name="draft" value="Save Draft"></cfif>
	                <input type="submit" name="save" value="Save Post">
	                <input type="submit" name="cancel" value="Cancel" class="cancel">
                </div>
            </form></div></cfoutput>