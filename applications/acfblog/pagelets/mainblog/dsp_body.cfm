<cfif StructKeyExists(request, "qryPost")>
            <div id="post"><cfoutput>
                <h2 class="top-align post-title">#request.qryPost.post_title#</h2>
                <p class="post-date">#dateFormat(request.qryPost.post_date, "DD Mmm YYYY")# <a href="/blog/#request.qryBlog.blog_name_user#/#request.qryPost.post_slug#">permalink</a></p>
                #request.qryPost.post_content#
            </cfoutput></div><!--- end post --->
            
            <!---
            <div id="related">
                <h2>Related Content</h2>
                <ul>
                    <li><a href="https://docs.google.com/document/edit?id=1VVSLS6_yM3kYH0yB3eY2bvKTCuZA-RD5dRl0AOkIfwI&hl=en#">Document API document on Google docs (protected)</a></li>
                </ul>
            </div>
			--->
            
            <div id="comments">
                <h2>Comments</h2>
                <p>Comments are currently not possible on this blog.</p>
            </div>
</cfif>