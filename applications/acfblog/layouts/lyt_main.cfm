<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><cfoutput>#getSpace("head")#</cfoutput><!--- <cfinclude template="pagelets/dsp_head.cfm"> --->
<script type="text/javascript">var jsHost='http://';document.write(unescape("%3Cscript src='" + jsHost + "assets.intranet.arup.com/common/javascript/ckeditor/3.2/ckeditor.js' type='text/javascript'%3E%3C/script%3E"));</script>
</head>

<body class="blue">
<div id="content">

    <div class="top-heading">
        <h1><cfoutput>#getSpace("top")#</cfoutput></h1>
    </div>

    <div id="maincontent">

        <div id="rightnav" class="col3 right">
        	<cfoutput>#getSpace("rightnav")#</cfoutput>
        </div>

        <div id="body" class="col6 left">
			<cfoutput>#getSpace("body")#</cfoutput>
            <!--- <cfinclude template="pagelets/mainblog/dsp_body.cfm"> --->
        </div><!--- /id=body --->

        <div id="footer"><cfoutput>#getSpace("footer")#</cfoutput><!--- <cfinclude template="pagelets/mainblog/dsp_footer.cfm"> ---></div>
    </div><!--- end maincontent --->
</div><!--- end content --->
<link type="text/css" rel="stylesheet" href="/plugins/syntaxhighlighter3/styles/shCore.css"></link>
<script language="javascript" src="/plugins/syntaxhighlighter3/scripts/shCore.js"></script>
<script language="javascript" src="/plugins/syntaxhighlighter3/scripts/shBrushColdFusion.js"></script>
<script language="javascript" src="/plugins/syntaxhighlighter3/scripts/shBrushXml.js"></script>
<script language="javascript">
// dp.SyntaxHighlighter.ClipboardSwf = '/flash/clipboard.swf';
SyntaxHighlighter.all('code');
</script>
</body>
</html>
