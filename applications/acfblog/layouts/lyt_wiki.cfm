<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><cfinclude template="pagelets/dsp_head.cfm"></head>

<body class="blue">
<div id="content" class="acb-wiki">

    <div class="top-heading">
        <h1><cfoutput>Nick's Wiki</cfoutput></h1>
    </div>
    
    <cfabort>
    
    <div id="maincontent">
    
        <div id="rightnav" class="col3 right">
            <cfinclude template="pagelets/wikimain/dsp_recent.cfm">
            <cfinclude template="pagelets/pods/dsp_downloads.cfm">
            
            <cfinclude template="pagelets/pods/dsp_admin.cfm">
        </div><!--- end rightnav --->
    
        <div id="body" class="col6 left">
			<cfinclude template="pagelets/wikimain/dsp_body.cfm">
        </div><!--- /id=body --->
        
        <div id="footer"><cfinclude template="pagelets/mainblog/dsp_footer.cfm"></div>
    </div><!--- end maincontent --->
</div><!--- end content --->
<link type="text/css" rel="stylesheet" href="/plugins/syntaxhighlighter3/styles/shCore.css"></link>
<script language="javascript" src="/plugins/syntaxhighlighter3/scripts/shCore.js"></script>
<script language="javascript" src="/plugins/syntaxhighlighter3/scripts/shBrushColdFusion.js"></script>
<script language="javascript" src="/plugins/syntaxhighlighter3/scripts/shBrushXml.js"></script>
<script language="javascript">
// dp.SyntaxHighlighter.ClipboardSwf = '/flash/clipboard.swf';
SyntaxHighlighter.all('code');
</script>
</body>
</html>
