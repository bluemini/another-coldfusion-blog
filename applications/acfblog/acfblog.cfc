<cfcomponent>
	<!--- this component is run by rhino.cfc immediately after loadConfiguration(), it therefore has no access to any of the output from extension cfc's --->

	<cffunction name="onAppStart" access="public" output="false">
		<!--- called here by rhino.cfc --->
	</cffunction>
	
	<!--- abstract class, this must be returned --->
	<cffunction name="getModules" access="public" output="false" returntype="struct">
		<cfset strResult = structNew()>
		<!--- getModules allows the application to control how it wants to interact with extensions, getModules returns a struct, if the struct
		has a single key called allowAll set to true then all possible extensions may be loaded, if a key denyAll is true then no extensions should 
		be loaded and if not present then the struct should contain a key for every extension to be loaded. If empty, denyAll=true is assumend.
		
		NB
		- The actual values of the keys is irrelevant
		- If both keys exist, then denyAll will apply. (based on this being the first structKeyExists() test --->
		<cfset strResult.allowAll = true>
		<cfreturn strResult>
	</cffunction>

</cfcomponent>
