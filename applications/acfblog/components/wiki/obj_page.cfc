<cfcomponent>

	<cffunction name="getPage" access="public" returntype="query">
		<cfargument name="wikiid" type="numeric" required="true">
		<cfargument name="pageid" type="string" required="false" default="0" hint="the id of the Page, if not supplied, show latest">
		<cfargument name="includeDraft" type="boolean" required="false" default="false">

		<!--- check the pageid and if not specified, get the highest value --->
		<cfif arguments.pageid EQ 0>
			<cfset arguments.pageid = getLatestpageid(wikiid, includeDraft)>
		</cfif>

		<!--- if the pageid is numeric, assume a id, otherwise assume a slug --->
		<cfif IsNumeric(pageid) AND pageid GT 0>
			<cfset myResult = getPageById(wikiid, pageid, includeDraft)>
		<cfelse>
			<cfset myResult = getPageBySlug(wikiid, pageid, includeDraft)>
		</cfif>

		<cfreturn myResult>
	</cffunction>


	<!--- access the data in the database --->
	<cffunction name="getPageById" access="public" returntype="query">
		<cfargument name="wikiid" type="numeric" required="true">
		<cfargument name="pageid" type="numeric" required="true">
		<cfargument name="includeDraft" type="boolean" required="false" default="false">

		<cfquery name="Page" datasource="blog">
			SELECT	*
			FROM	t_pages
			WHERE	page_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#pageid#">
			  AND   page_wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#wikiid#">
			<cfif arguments.includeDraft>  AND	page_valid != 0<cfelse>  AND   page_valid = 1</cfif>
		</cfquery>

		<cfreturn Page>
	</cffunction>

	<!--- access the data in the database --->
	<cffunction name="getPageBySlug" access="public" returntype="query">
		<cfargument name="wikiid" type="numeric" required="true">
		<cfargument name="Pageslug" type="string" required="true">
		<cfargument name="includeDraft" type="boolean" required="false" default="false">

		<cfquery name="Page" datasource="blog">
			SELECT	*
			FROM	t_pages
			WHERE	page_slug = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Pageslug#">
			  AND   page_wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#wikiid#">
			<cfif arguments.includeDraft>  AND	page_valid != 0<cfelse>  AND   page_valid = 1</cfif>
		</cfquery>

		<cfreturn Page>
	</cffunction>



	<!--- get the last X Pages --->
	<cffunction name="getLatestPages" access="public" returntype="query">
		<cfargument name="wikiid" type="numeric" required="true">
		<cfargument name="recentNum" type="numeric" required="false" default="5">
		<cfargument name="includeDraft" type="boolean" required="false" default="false">
		<cfargument name="getFullData" type="boolean" required="false" default="false">

		<cfquery name="Pages" datasource="blog">
			SELECT	page_title, page_id, page_valid, page_slug
			<cfif arguments.getFullData>		,page_content, page_date</cfif>
			FROM	t_pages
			WHERE	page_wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#wikiid#">
			<cfif arguments.includeDraft>  AND	page_valid != 0<cfelse>  AND   page_valid = 1</cfif>
			ORDER BY page_id DESC
			LIMIT	#recentNum#
		</cfquery>

		<cfreturn Pages>
	</cffunction>

	<!--- get the latest valid Page --->
	<cffunction name="getLatestPageId" access="private" returntype="numeric" description="get the latest Page id">
		<cfargument name="wikiid" type="numeric" required="true">

		<cfset var latestpageid = -1>

		<cfquery name="maxId" datasource="blog">
			SELECT	max(page_id) page_id
			FROM	t_pages
			WHERE	page_valid = 1
		</cfquery>

		<cfif maxId.page_id IS NOT ""><cfset latestpageid = maxId.page_id></cfif>

		<cfreturn latestpageid>
	</cffunction>

	<!--- save an new page to the database --->
	<cffunction name="savePage" access="public" returntype="string" description="Adds a new blog Page to the database">
		<cfargument name="data" type="struct" required="true">
		<cfargument name="returnValue" type="string" required="false" default="id" hint="Allows the specification of the slug or id, if not 'slug' then 'id' will be assumed">

		<cfset var pageid = 0>
		<cfset var slug = "">
		<cfset var newPage = true>
		<cfset var PageVersion = 1>

		<cfif StructKeyExists(data, "Page")>
			<cfset pageid = data.Page>
			<cfset newPage = false>
		<cfelse>
			<cfset pageid = getNextPageId()>
		</cfif>

		<!--- validate that the page is supplied --->
		<cfif NOT StructKeyExists(data, "Pagetitle")
			OR NOT StructKeyExists(data, "wikiid")
			OR NOT IsNumeric(data.wikiid)
			OR NOT StructKeyExists(data, "Pagebody")>
			<cfreturn 0>
		</cfif>

		<!--- create the slug --->
		<cfset slug = replace(lcase(data.Pagetitle), " ", "_", "ALL")>

		<cftransaction isolation="repeatable_read">
			<cfif NOT newPage>
				<cfquery name="version" datasource="blog">
					SELECT	max(page_version)+1 as nextid
					FROM	t_pages
					WHERE	page_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#pageid#">
					  AND	page_wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#data.wikiid#">
				</cfquery>
				<cfset PageVersion = version.nextid>
			</cfif>

			<cfquery datasource="blog">
				INSERT INTO t_pages
					(page_id,
					page_title,
					page_slug,
					page_date,
					page_comment_count,
					page_date_modified,
					page_content,
					page_valid,
					page_wiki_id,
					page_version,
					page_editor_id)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_integer" value="#pageid#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#data.Pagetitle#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#slug#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
					0,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#data.Pagebody#">,
					<cfif StructKeyExists(form, "save")>1<cfelse>2</cfif>,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#data.wikiid#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#PageVersion#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#cookie.acfbid#">)
			</cfquery>

			<!--- if this is an edit, then invalidate the old Pages --->
			<cfif NOT newPage>
				<cfquery datasource="blog">
					UPDATE	t_pages
					SET		page_valid = 0
					WHERE	page_wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#data.wikiid#">
					  AND	page_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#pageid#">
					  AND	page_version < <cfqueryparam cfsqltype="cf_sql_integer" value="#PageVersion#">
				</cfquery>
			</cfif>
		</cftransaction>

		<cfif arguments.returnValue IS "slug">
			<cfreturn slug>
		<cfelse>
			<cfreturn pageid>
		</cfif>
	</cffunction>

	<!--- save an new page to the database --->
	<cffunction name="getNextPageId" access="private" returntype="numeric" description="Fetches the next page id from the database">
		<cfset var seq = queryNew("")>

		<cftransaction isolation="repeatable_read">
			<cfquery datasource="blog">UPDATE sequences SET page_seq = page_seq + 1</cfquery>
			<cfquery name="seq" datasource="blog">
				SELECT page_seq FROM sequences
			</cfquery>
		</cftransaction>

		<cfreturn seq.page_seq>
	</cffunction>

</cfcomponent>