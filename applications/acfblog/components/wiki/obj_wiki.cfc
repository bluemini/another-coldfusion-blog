<cfcomponent>

	<cfset settings = StructNew()>
    <cfset settings.UserResolutionWebServiceURL = "http://webservices.intranet.arup.com/aruppeople/aruppeople.cfc?wsdl">
    <cfset settings.UserResolutionWebServiceUserArgument = "iStaffId">
    <cfset settings.UserResolutionWebServiceMethod = "getStaffInfo">

	<cffunction name="getWiki" access="public" returntype="query">
    	<cfargument name="wikiid" type="string" required="true">
        
        <cfset var qryWiki = queryNew("")>
        
        <cfquery name="qryWiki" datasource="blog">
        	SELECT	*, replace(wiki_owner_name, " ", "") as wiki_name_user
            FROM	t_wikis
            <cfif isNumeric(wikiid)>
            WHERE	wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#wikiid#">
            <cfelse>
            WHERE	Replace(wiki_owner_name, " ", "") = <cfqueryparam cfsqltype="cf_sql_varchar" value="#wikiid#">
            </cfif>
        </cfquery>
        
        <!--- <cfif qryBlog.recordCount GT 0>
        	<!--- lookup the staff id and division against the web service --->
            <cfset UserResolutionArgs = StructNew()>
            <cfset UserResolutionArgs[settings.UserResolutionWebServiceUserArgument] = qryBlog.blog_user_id>
            <cfinvoke webservice="#settings.UserResolutionWebServiceURL#" method="#settings.UserResolutionWebServiceMethod#" argumentcollection="#UserResolutionArgs#" returnvariable="userInfo"></cfinvoke>
        </cfif> --->
        
        <cfreturn qryWiki>
    </cffunction>

    <cffunction name="getRecentPages" access="public" returntype="query">
    	<cfargument name="wikiid" type="numeric" required="true">
    	<cfargument name="recentCount" type="numeric" required="false" default="10">
        
        <cfquery name="qryWiki" datasource="blog">
        	SELECT	*
            FROM	t_pages
            WHERE	page_wiki_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.wikiid#">
              AND	page_valid = 1
            GROUP BY page_id
            ORDER BY page_date DESC
            LIMIT   #recentCount#
        </cfquery>
        
        <cfreturn qryWiki>
    </cffunction>
    
    <cffunction name="isEditor" access="public" returntype="boolean" output="false">
    	<cfargument name="userid" type="numeric" required="true">
        <cfreturn true>
    </cffunction>
    
    <cffunction name="isViewer" access="public" returntype="boolean" output="false">
    	<cfargument name="userid" type="numeric" required="true">
        <cfreturn true>
    </cffunction>
 
 </cfcomponent>