<cfcomponent>

	<cfset settings = StructNew()>
    <cfset settings.UserResolutionWebServiceURL = "http://webservices.intranet.arup.com/aruppeople/aruppeople.cfc?wsdl">
    <cfset settings.UserResolutionWebServiceUserArgument = "iStaffId">
    <cfset settings.UserResolutionWebServiceMethod = "getStaffInfo">

	<cffunction name="getBlog" access="public" returntype="query">
    	<cfargument name="blogid" type="string" required="true">
        
        <cfset var qryBlog = queryNew("")>
        
        <cfquery name="qryBlog" datasource="blog">
        	SELECT	blog_id, blog_name, blog_user_name, blog_about_text,
					blog_user_id, blog_user_division, 
					replace(blog_user_name, " ", "") as blog_name_user
            FROM	t_blogs
            <cfif isNumeric(blogid)>
            WHERE	blog_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#blogid#">
            <cfelse>
            WHERE	Replace(blog_user_name, " ", "") = <cfqueryparam cfsqltype="cf_sql_varchar" value="#blogid#">
            </cfif>
        </cfquery>
        
        <!--- <cfif qryBlog.recordCount GT 0>
        	<!--- lookup the staff id and division against the web service --->
            <cfset UserResolutionArgs = StructNew()>
            <cfset UserResolutionArgs[settings.UserResolutionWebServiceUserArgument] = qryBlog.blog_user_id>
            <cfinvoke webservice="#settings.UserResolutionWebServiceURL#" method="#settings.UserResolutionWebServiceMethod#" argumentcollection="#UserResolutionArgs#" returnvariable="userInfo"></cfinvoke>
        </cfif> --->
        
        <cfreturn qryBlog>
    </cffunction>

    <!--- translate a  --->
    <cffunction name="getBlogIdByName" access="public" returntype="numeric">
    	<cfargument name="blogname" type="string" required="true">
		
		<cfset var blogid = 0>
        
        <cfquery name="blog" datasource="blog">
        	SELECT	blog_id
            FROM	t_blogs
            WHERE	Replace(blog_user_name, " ", "") = <cfqueryparam cfsqltype="cf_sql_varchar" value="#blogname#">
        </cfquery>
		
		<cfif blog.RecordCount GT 0>
			<cfset blogid = blog.blog_id>
		</cfif>
        
        <cfreturn blogid>
    </cffunction>
	
</cfcomponent>