<cfsilent>
<!--- DEFAULT PARAMETERS --->

<!--- create the roots struct key if it doesn't exist --->
<cfscript>
	// Initialize the Rhino singleton and applications scope storage
	if (NOT structKeyExists(application, "rhino") OR structKeyExists(url, "rhinoappstart") OR NOT application.rhino.isInitialised()) {
		application.rhino = createObject("component", "rhino_core.packages.rhino");
		application.rhino.init();
	}

	// set a reference to the rhino singleton into the request scope
	request.rhino = application.rhino;
	
	// create the struct to hold the merged url and form variables.
	attributes = structNew();

	// set up all the rhino objects into the request scope
	request.page = createObject("component", "packages.page").init(request.rhino);
	request.data = createObject("component", "packages.data").init(request.rhino, attributes);
	// request.asset = createObject("component", "packages.asset").init(request.rhino, attributes);
	// request.space = createObject("component", "packages.space").init(request.rhino, request.page, request.asset);
	request.wire = createObject("component", "packages.wire").init(request.rhino, attributes);
	request.error = createObject("component", "packages.error");

	application.rhino.setupRhino();

	// merge url and form variables into the attributes (mapped into assets.attributes 
	// so available to all included templates under rhino
	request.page.mergeScopes(url, form, attributes);

	// call the onAppStartPostParse event
	request.rhino.getEvent().callHandler(sEvent="onRequestStartPostParse", attributes=attributes);

	// after all other scopes have been started if needed, start the page
	request.page.processRequest(request);

</cfscript>
</cfsilent>