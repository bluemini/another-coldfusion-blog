<cffunction name="getData" access="public" output="false">
    <cfargument name="dataset" type="string">
    <cfargument name="type" type="string">
    <cfargument name="cache" type="boolean" default="true">
    <cfreturn request.data.getData(dataset, type, cache)>
</cffunction>

<cffunction name="getObject" access="public" returntype="any" output="false">
    <cfargument name="objectName" type="string" hint="Name of the component to be returned, provide path relative to components directory using dots as separators">
    <cfargument name="type" type="string" required="no" default="local">
    <cfargument name="cache" type="boolean" required="no" default="true" hint="specify whether to use a previous cached instance and whether to store this instance in cache">
    <cfreturn request.data.getObject(objectName, type, cache)>
</cffunction>

<cffunction name="cacheObject" access="public" output="false" hint="allows for data objects to be pushed onto the cache stack for this request">
    <cfargument name="objIncoming" required="true">
    <cfargument name="sName" required="true" type="string">
    <cfreturn request.data.cacheObject(objIncoming, sName)>
</cffunction>

<cffunction name="wireObject" output="false">
	<cfargument name="baseComponentName" required="true">
	<cfargument name="useCache" required="false" default="true">
	<cfargument name="callInit" required="false" type="boolean" default="true">
	<cfreturn request.data.wireObject(baseComponentName, useCache, callInit)>
</cffunction>