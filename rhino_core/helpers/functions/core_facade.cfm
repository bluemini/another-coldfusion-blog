<cffunction name="getRhinoIndexFile" access="public" returntype="string" output="false">
    <cfreturn request.rhino.getRhinoIndexFile()>
</cffunction>
    
<!--- provides access to elements of the locals.xml file in the application config.--->
<cffunction name="getLocal" access="public" returntype="string" output="false">
    <cfargument name="sLocal" type="string" required="true">
    <cfargument name="throwOnNotFound" type="boolean" required="false" default="false">
    <cfreturn request.rhino.getLocal(sLocal, throwOnNotFound)>
</cffunction>
    
<cffunction name="getLog" access="public" output="false">
    <cfreturn request.rhino.getLog()>
</cffunction>
<cffunction name="getEvent" access="public" output="false">
    <cfreturn request.rhino.getEvent()>
</cffunction>

<!--- provide framework access to the application's name --->
<cffunction name="getRhinoName" access="public" output="false" returntype="string">
    <cfreturn request.rhino.getName()>
</cffunction>
    
<!--- return the 'state' string (defaults to 'DEV') for the current application. --->
<cffunction name="getRhinoState" access="public" output="false" returntype="string">
    <cfreturn request.rhino.getState()>
</cffunction>