<!--- before including index, parse out the url variables if possible. These are stored
in the URL (querystring) scope, since this page has been rewritten by IIS --->
<cfset fullPath = getToken(cgi.query_string, 2, ";")>
<cfset fullPath = replace(fullPath, "?", "/", "All")>
<cfset extPath = REReplace(fullPath, "[Hh][Tt][Tt][Pp][Ss]*://[^/]+", "")>

<cfset objAppBase = application.rhino.getEvent().getExtension(application.rhino.getName())>
<cfset sesAttempt = objAppBase.sesParser(extPath)>

<cfif sesAttempt.success>
	<cfset structAppend(url, sesAttempt, false)>
	<cfinclude template="../index.cfm">
<cfelse>
	<cfheader statuscode="404" statustext="Page Not Found">
	<html><head><title>Page Not Found</title></head><body><h1>Page Not Found</h1></body></html>
</cfif>

