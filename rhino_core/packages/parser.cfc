<cfcomponent>
    
    <cfset instance.modulename = "">
    
    <cffunction name="init" access="public" output="false">
        <cfargument name="refRhino" type="rhino" required="true">
        <cfset instance.rhino = arguments.refRhino>
        <cfreturn this>
    </cffunction>

	<cffunction name="loadRhinoConfig" access="public" output="false">
        <cfset var configFile = "">
        
		<!--- first we load the rhino config file --->
		<cfset instance.rhino.getLog().addEntry("Loading Rhino configuration")>
        <cffile action="read" file="#instance.rhino.roots().rhinoRootPath#helpers/config/config.xml" variable="configFile">
        
        <cftry>
            <cfset instance.rhino.setConfig(this.getConfig(configFile))>
            <cfcatch><cfdump var="#instance.rhino#"><cfabort></cfcatch>
        </cftry>
	</cffunction>

	<cffunction name="loadConfig" access="public" returntype="struct" output="false">
		<cfargument name="sModule" type="string" required="yes">
        <cfargument name="sScope" type="string" required="false" default="Application">
        
		<cfset var hasWiring = true>
		
        <!--- determine the file location to load the configuration from --->
        <cfif arguments.sScope IS "CoreExtension">
            <cfset sRoot = "#instance.rhino.roots().rhinoRootPath#/extensions/#sModule#/">
        <cfelseif arguments.sScope IS "AppExtension">
            <cfset sRoot = "#instance.rhino.roots().applicationRootPath#/extensions/#sModule#/">
		<cfelseif arguments.sScope IS "Application">
			<cfset sRoot = "#instance.rhino.roots().applicationRootPath#/">
        <cfelse>
            <cfset instance.rhino.getLog().addEntry("[Parser] Unable to load configuration or '#arguments.sModule#', the scope passed in '#arguments.sScope#' is not 'appextension' or 'coreextension'", 2)>
            <cfreturn>
        </cfif>
		
		<!--- process the current template path to evaluate the location of the config files --->
		<cftry>
			
			<!--- first attempt to load an .xml version of the file, however, if for security
			xml files are not uploaded, it will attempt to locate the files with .cfm extensions. --->
			<cfset instance.rhino.getLog().addEntry("[Parser] Entering locals reading")>
			<cfif fileExists("#sRoot#config/locals-#instance.rhino.getState()#.xml")>
				<cfset instance.rhino.getLog().addEntry("[Parser] Reading '#instance.rhino.getState()#' locals")>
				<cffile action="read" file="#sRoot#config/locals-#instance.rhino.getState()#.xml" variable="instance.xmlfiles.locals">
			<cfelseif fileExists("#sRoot#config/locals-#instance.rhino.getState()#.cfm")>
				<cfset instance.rhino.getLog().addEntry("[Parser] Reading '#instance.rhino.getState()#' locals")>
				<cffile action="read" file="#sRoot#config/locals-#instance.rhino.getState()#.cfm" variable="instance.xmlfiles.locals">
			<cfelseif fileExists("#sRoot#config/locals.xml")>
				<cfset instance.rhino.getLog().addEntry("[Parser] Reading the global locals XML file")>
				<cffile action="read" file="#sRoot#config/locals.xml" variable="instance.xmlfiles.locals">
			<cfelseif fileExists("#sRoot#config/locals.cfm")>
				<cfset instance.rhino.getLog().addEntry("[Parser] Reading the global locals CFM file")>
				<cffile action="read" file="#sRoot#config/locals.cfm" variable="instance.xmlfiles.locals">
			<cfelse>
				<cfthrow message="locals configuration file cannot be found (have checked for both locals.xml and locals.cfm)">
			</cfif>
			
			<cfif fileExists("#sRoot#config/pages.xml")>
				<cffile action="read" file="#sRoot#config/pages.xml" variable="instance.xmlfiles.pages">
			<cfelseif fileExists("#sRoot#config/pages.cfm")>
				<cffile action="read" file="#sRoot#config/pages.cfm" variable="instance.xmlfiles.pages">
			<cfelse>
				<cfthrow message="pages configuration file cannot be found (have checked for both pages.xml and pages.cfm)">
			</cfif>
			
			<cfif fileExists("#sRoot#config/controllers.xml")>
				<cffile action="read" file="#sRoot#config/controllers.xml" variable="instance.xmlfiles.controllers">
			<cfelseif fileExists("#sRoot#config/controllers.cfm")>
				<cffile action="read" file="#sRoot#config/controllers.cfm" variable="instance.xmlfiles.controllers">
			<cfelse>
				<cfthrow message="controller configuration file cannot be found (have checked for both controllers.xml and controllers.cfm)">
			</cfif>
			
			<cfif fileExists("#sRoot#config/datasets.xml")>
				<cffile action="read" file="#sRoot#config/datasets.xml" variable="instance.xmlfiles.datasets">
			<cfelseif fileExists("#sRoot#config/datasets.cfm")>
				<cffile action="read" file="#sRoot#config/datasets.cfm" variable="instance.xmlfiles.datasets">
			<cfelse>
				<cfthrow message="datasets configuration file cannot be found (have checked for both datasets.xml and datasets.cfm)">
			</cfif>
			
			<cfif fileExists("#sRoot#config/wire.xml")>
				<cffile action="read" file="#sRoot#config/wire.xml" variable="instance.xmlfiles.wires">
			<cfelseif fileExists("#sRoot#config/wire.cfm")>
				<cffile action="read" file="#sRoot#config/wire.cfm" variable="instance.xmlfiles.wires">
			<cfelse>
				<cfset instance.rhino.getLog().addEntry("No wire definitions found")>
				<cfset hasWiring = false>
			</cfif>
			
			<cfcatch>
				<cfif structKeyExists(cfcatch, "rootCause")
					AND structKeyExists(cfcatch.rootCause, "type")
						AND cfcatch.rootCause.type IS "java.io.FileNotFoundException">
					<cfthrow message="The CFRhino configuration file '#cfcatch.file#' cannot be found. Please ensure it exists.">
				<cfelse>
					<cfrethrow>
				</cfif>
			</cfcatch>
		</cftry>

		<!--- call the various parsers to get all data --->
		<cfset strConfig = structNew()>
		<cfset strConfig.Locals = getLocals(arguments.sModule)>
		<cfset strConfig.Datasets = getDatasets(arguments.sModule)>
		<cfset strConfig.Pages = getPages(arguments.sModule)>
		<cfset strConfig.Pages = processPages(strConfig.pages)>
		<cfset strConfig.Controllers = getControllers(arguments.sModule)>
		<cfif hasWiring><cfset strConfig.Wires = getWiring(arguments.sModule)></cfif>

		<cfreturn strConfig>
	</cffunction>
		 
	<cffunction name="getConfig" access="public">
		<cfargument name="sFileConfig" type="string" required="yes">
        
        <cfset var strConfig = structNew()>
        
		<!--- parse the config file for the application --->
		<cfset xmlAppConfig = xmlParse(sFileConfig)>
		<cfset currentAppConfig = xmlSearch(xmlAppConfig, "/cfapp-config/cfapp[@id='rhino']")>
		<cfif arrayLen(currentAppConfig)>
			<cfset currentAppConfig = currentAppConfig[1]>
			
            <!--- Other parameters --->
			<cfloop index="i" from="1" to="#arrayLen(currentAppConfig['config-param'])#">
				<cfset xmlElement = currentAppConfig['config-param'][i]>
				<cfset strConfig[xmlElement['param-name'].xmlText] = xmlElement['param-value'].xmlText>
                <cfset instance.rhino.getLog().addEntry("Adding config.xml param, #xmlElement['param-name'].xmlText#: #xmlElement['param-value'].xmlText#", "4")>
			</cfloop>
			
            <!--- set some last resort defaults for rhinoIndex, defaultLayout --->
			<cfif NOT structKeyExists(strConfig, "rhinoindex")><cfset strConfig.rhinoindex = "index"></cfif>
			<cfif NOT structKeyExists(strConfig, "defaultLayout")><cfset strConfig.defaultLayout = "home"></cfif>
			<cfif NOT structKeyExists(strConfig, "rhino_log")><cfset strConfig.rhino_log = false></cfif>
			<cfif NOT structKeyExists(strConfig, "rhino_state")><cfset strConfig.rhino_state = "LIVE"></cfif>
			<cfif NOT structKeyExists(strConfig, "reparse_dev") OR NOT isBoolean(strConfig.reparse_dev)><cfset strConfig.reparse_dev = true></cfif>
            
        <cfelse>
			<cfset instance.rhino.getLog().addEntry("No configuration found for Rhino", 2)>
		</cfif>
        
        <cfreturn strConfig>
	</cffunction>

	<cffunction name="getLocals" access="private" returntype="struct" output="false">
		<cfargument name="sCFApp" type="string" required="yes">
		
		<cfset var xmlAppLocals = xmlNew(false)>
		<cfset var currentAppLocals = arrayNew(1)>
		<cfset var locals = structNew()>
		<cfset var i = 0>
		<cfset var xmlElement = xmlNew(false)>
		<cfset var namespace = "">
		
		<!--- parse the locals file for the setting of instance relevant only to the current app --->
		<cfset xmlAppLocals = xmlParse(instance.xmlfiles.locals)>
		<cfset namespace = getNamespace(xmlAppLocals)>
		<cfif namespace IS NOT ""><cfset namespace = "#namespace#:"></cfif>
		<!--- search for matching locals definitions for the current application --->
		<cfset currentAppLocals = xmlSearch(xmlAppLocals, "/#namespace#cfapp-locals/#namespace#cfapp[@id='#sCFApp#']")>
		<cfif arrayLen(currentAppLocals)>
			<cfset currentAppLocals = currentAppLocals[1]>
			
			<!--- provide support for the old format of XML --->
			<cfif structKeyExists(currentAppLocals, "local-param")>
				<!--- Virtual path parameters --->
				<cfloop from="1" index="i" to="#arrayLen(currentAppLocals['local-param'])#">
					<cfset xmlElement = currentAppLocals['local-param'][i]>
					<cfset locals[xmlElement['param-name'].xmlText] = xmlElement['param-value'].xmlText>
				</cfloop>
			
			<!--- provide support for the new format of XML --->
			<cfelseif structKeyExists(currentAppLocals, "#namespace#param")>
				<!--- Virtual path parameters --->
				<cfloop from="1" index="i" to="#arrayLen(currentAppLocals['#namespace#param'])#">
					<cfset xmlElement = currentAppLocals['#namespace#param'][i]>
					<cfset locals[xmlElement.xmlAttributes.name] = xmlElement.xmlText>
				</cfloop>
			<cfelse>
				Cannot find application locals namespace.<cfdump var="#currentAppLocals#"><cfabort>
			</cfif>
			
		<cfelse>
			<cfset instance.rhino.getLog().addEntry("No locals definitions found for '#arguments.sCFapp#'", 3)>
		</cfif>
		<cfreturn locals>
	</cffunction>
		
	<!--- Parses the datasets.xml file. --->
	<cffunction name="getDatasets" access="private" returntype="struct" output="false">
		<cfargument name="sCFApp" type="string" required="yes">
		
		<cfset var strDatasets = structNew()>
		<cfset var xmlDatasets = xmlParse(instance.xmlfiles.datasets)>
		<cfset var arrDatasets = arrayNew(1)>
		<cfset var strDataset = structNew()>
		<cfset var node = "">
		<cfset var nodeAttributes = "">
		<cfset var namespace = getNamespace(xmlDatasets)>
		
		<cfif namespace IS NOT ""><cfset namespace = "#namespace#:"></cfif>
		<cfset arrDatasets = xmlSearch(xmlDatasets, "/#namespace#cfapp-datasets/#namespace#cfapp[@id='#sCFApp#']/#namespace#dataset")>
		
		<!--- component properties --->
		<cfloop index="iDataset" from="1" to="#arrayLen(arrDatasets)#">
			<cfset strDataset = structNew()>
			<!--- get the basic dataset details --->
			<cfif structKeyExists(arrDatasets[iDataset].xmlAttributes, "name")><cfset strDataset.name = arrDatasets[iDataset].xmlAttributes.name><cfelse><cfthrow message="'Name' is undefined for dataset"></cfif>
			<cfif structKeyExists(arrDatasets[iDataset].xmlAttributes, "method")><cfset strDataset.method = arrDatasets[iDataset].xmlAttributes.method><cfelse><cfthrow message="'Method' is undefined for dataset"></cfif>
			<cfif structKeyExists(arrDatasets[iDataset].xmlAttributes, "type")><cfset strDataset.type = arrDatasets[iDataset].xmlAttributes.type><cfelse><cfthrow message="'Type' is undefined for dataset"></cfif>
			<cfif structKeyExists(arrDatasets[iDataset].xmlAttributes, "location")><cfset strDataset.location = arrDatasets[iDataset].xmlAttributes.location>
				<cfelseif structKeyExists(arrDatasets[iDataset].xmlAttributes, "module")><cfset strDataset.module = arrDatasets[iDataset].xmlAttributes.module>
				<cfelse><cfthrow message="'Location' is undefined for dataset #strDataset.name# in #scfapp#"></cfif>
			
			<!--- make the dataset name fully qualified if it isn't --->
			<cfif getToken(strDataset.name, 1, ".") IS NOT arguments.sCFApp><cfset strDataset.name = sCFApp & "." & strDataset.name></cfif>
			
			<!--- the dataset element MAY have children which specifies the parameters it requires --->
			<cfset strDataset.parameters = arrayNew(2)>
			
			<cfif arrayLen(arrDatasets[iDataset].xmlChildren)>
				<cfloop from="1" to="#arrayLen(arrDatasets[iDataset].xmlChildren)#" index="iChild">

					<!--- initialise the array, node and nodeAttributes --->
					<cfset arraySet(strDataset.parameters[iChild], 1, 6, 0)>
					<cfset node = arrDatasets[iDataset].xmlChildren[iChild]>
					<cfset nodeAttributes = node.xmlAttributes>

					<!--- get the attribute name (this is the name of the value passed to the function) --->
					<cfif structKeyExists(arrDatasets[iDataset].xmlChildren[iChild].xmlAttributes, "name")>
						<cfset strDataset.parameters[iChild][2] = arrDatasets[iDataset].xmlChildren[iChild].xmlAttributes.name>
					<cfelse>
						<cfthrow message="'Name' is undefined for a dataset parameter">
					</cfif>
					
					<!--- record where the parameter's scope (url/form/value) and the key into that scope where the value is found --->
					<cfif structKeyExists(nodeAttributes, "scope") AND structKeyExists(nodeAttributes, "inputname")>
						<cfset strDataset.parameters[iChild][1] = nodeAttributes.inputname>
						<cfset strDataset.parameters[iChild][3] = nodeAttributes.scope>
					<cfelseif structKeyExists(nodeAttributes, "scope") AND structKeyExists(nodeAttributes, "value")>
						<cfset strDataset.parameters[iChild][1] = nodeAttributes.value>
						<cfset strDataset.parameters[iChild][3] = "value">
					<cfelse>
						<cfdump var="#node#"><cfabort>
						<cfthrow message="Attributes cannot be found for a dataset parameter which fit requirements" detail="Dataset required parameters must contain either<ul><li>name, scope, inputname</li><li>name, scope, value</li></ul>">
					</cfif>
					
					<!--- check if the parameter is declared as 'required' in the XML and if not, set index[6] to be true --->
					<cfif structKeyExists(nodeAttributes, "required")>
						<cfset fRequired = lcase(nodeAttributes.required)>
						<cfif fRequired IS "false" OR fRequired IS "no" OR fRequired EQ "0">
							<cfset strDataset.parameters[iChild][6] = 0>
						<cfelseif fRequired IS "true" OR fRequired IS "yes" OR fRequired EQ "1">
							<cfset strDataset.parameters[iChild][6] = 1>
						<cfelse>
							<cfthrow message="'Required' attribute contains an unknown value, use true/false, yes/no or 1/0." detail="A parameter has the 'required' attribute set but the value is incorrect.">
						</cfif>
					<cfelse>
						<cfset strDataset.parameters[iChild][6] = 1>
					</cfif>
					
					<!--- check if the parameter has a corrective action if the value does not exist and record in element 4
					Currently only cflocation responses can be handled --->
					<cfif structKeyExists(nodeAttributes, "corrective")>
						<cfset sCorrectiveAction = nodeAttributes.corrective>
						<cfif listFindNoCase(sCorrectiveAction, "location", ":")>
							<cfset strDataset.parameters[iChild][4] = "location">
							<cfset strDataset.parameters[iChild][5] = listGetAt(sCorrectiveAction, 2, ":")>
						<cfelseif listFindNoCase(sCorrectiveAction, "default", ":")>
							<cfset strDataset.parameters[iChild][4] = "default">
							<cfset strDataset.parameters[iChild][5] = listGetAt(sCorrectiveAction, 2, ":")>
						</cfif>
					</cfif>
					
				</cfloop>

			</cfif>
			<!--- and set the structure into the appLayouts parent --->
			<cfset strDatasets[strDataset.name] = strDataset>
		</cfloop>
		<cfreturn strDatasets>
	</cffunction>
	
	<cffunction name="getPages" access="private" returntype="struct" output="false">
		<cfargument name="sCFApp" type="string" required="yes">

		<cfset var appPages = structNew()>
		<cfset var xmlAppPages = xmlParse(instance.xmlfiles.pages)>
		<cfset var namespace = getNamespace(xmlAppPages)>
		<cfset var arrAppPages = arrayNew(1)>
		<cfset var startTick = getTickCount()>
		<!---
		<cfset var construct = 1>					<!--- tracks the current construct being parsed for a page definition --->
		<cfset var constructVersion = "">			<!--- tracks the value of the 'version' attribute in a construct --->
		<cfset var constructVersionDefault = false>	<!--- tracks if we've already recorded a non-versioned version of the page --->
		
		<cfset var spaceDetails = StructNew()>
		<cfset var pageletDetails = StructNew()>
		<cfset var arrConstruct = ArrayNew(1)>
		<cfset var arrConstructLayouts = ArrayNew(1)>
		<cfset var arrConstructPagelets = ArrayNew(1)>
		<cfset var arrConstructModules = ArrayNew(1)>
		
		<cfset var sPageName = "">
		<cfset var sFQPageName = "">
		<cfset var iTempOrder = 0>
		<cfset var pageDeleted = false>
		<cfset var pageWraps = false>
		--->
		<cfset var pagesTotal = 0>
		
		<cfif namespace IS NOT ""><cfset namespace = namespace & ":"></cfif>
		<cfset arrAppPages = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page")>
        
		<cfset pagesTotal = arrayLen(arrAppPages)>
        <cfset instance.rhino.getLog().addEntry("there are #pagesTotal# pages in #sCFApp#.")>
		
        <cfif pagesTotal>
	
			<!--- PAGE parameters --->
			<cfloop index="i" from="1" to="#pagesTotal#">
				<cfset xmlElement = arrAppPages[i]>
					
				<cfset constructVersionDefault = False>
				<cfset sPageName = xmlElement.xmlAttributes.name>
				<cfset pageDeleted = False>
				<cfset pageWraps = False>
				
				<cfif getToken(sPageName, 1, ".") IS NOT sCFApp><cfset sFQPageName = sCFApp & "." & sPageName><cfelse><cfset sFQPageName = sPageName></cfif>
				
                <!--- record any special flag tags (wrap, deleted) --->
				<cfif structKeyExists(xmlElement, "wrap")><cfset pageWraps = xmlElement.wrap><cfelse><cfset pageWraps = "yes"></cfif>
                <cfif structKeyExists(xmlElement.xmlAttributes, "deleted") AND isBoolean(xmlElement.xmlAttributes.deleted)>
					<cfset pageDeleted = xmlElement.xmlAttributes.deleted>
				</cfif>
				
				<!--- get the build spec --->
				<cfset arrConstruct = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct")>
				
				<!--- if the page is deleted, we don't go any further --->
				<cfif NOT pageDeleted>
				
					<!--- parse page details for each construct --->
					<cfloop from="1" to="#ArrayLen(arrConstruct)#" index="construct">
					
						<cfset appPage = structNew()>
						<cfset appPage.layouts = arrayNew(1)>
						<cfset appPage.pagelets = structNew()>
						<cfset appPage.datasets = arrayNew(1)>
						<cfset appPage.modules = structNew()>
						<cfset appPage.params = structNew()>
						<cfset appPage.space = structNew()>
						<cfset appPage.spacePagelets = structNew()>
						<!--- cfset appPage.construct = structNew() --->
						<cfset appPage.head = arrayNew(1)>
						<cfset appPage.deleted = False>

						<!--- store the current construct version name. If we've not yet dealt with
						a versionless construct and no version is defined, then parse one, otherwise 
						throw an error. --->
						<cfif StructKeyExists(arrConstruct[construct].xmlAttributes, "version")>
							<cfset constructVersion = arrConstruct[construct].xmlAttributes.version>
						<cfelseif NOT constructVersionDefault>
							<cfset constructVersion = "default">
							<cfset constructVersionDefault = true>
						<cfelse>
							<cfthrow message="You cannot have multiple constructs defined in the XML that do not explicitly define a 'version'"
									detail="A page '#sPageName#' has multiple construct sections in pages.xml, however, more than one has either 
										no 'version' attribute or the same value for the 'version' is repeated. Ensure that all values for the
										version attribute are unique within a page. Note: values may be commma separated and in this case, each
										term of the list is considered a separate version identifier">
						</cfif>
					
						<!--- fetch the data for the particular construct --->
						<cfset arrConstructLayouts = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct[#construct#]/#namespace#layout")>
						<cfset arrConstructPagelets = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct[#construct#]/#namespace#pagelet")>
						<cfset arrConstructModules = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct[#construct#]/#namespace#module")>
		
						<!--- process the layout --->
						<cfif arrayLen(arrConstructLayouts) EQ 0>
		                    <cfset instance.rhino.getLog().addEntry("Unable to find any layouts for page #sPageName#")>
		                    <cfthrow message="No layout specified" detail="A page must have at least one layout specified. Rhino was looking in the '#sCFApp#' pages.xml for #sPageName# and could find no matching pages.">
						<cfelse>
							<cfset arrayAppend(appPage.layouts, arrConstructLayouts[1].xmlAttributes.name)>
		                </cfif>
			
						<!--- process the pagelets --->
		                <cfif arrayLen(arrConstructPagelets) EQ 0>
		                    <cfthrow message="No pagelets found in page #sPageName#">
		                </cfif>
						<cfloop from="1" to="#arrayLen(arrConstructPagelets)#" index="iPagePagelet">
							<!--- build a list of required datasets for each page based on the pagelets that are defined for it --->
		                    
		                    <cfset xmlPagelet = arrConstructPagelets[iPagePagelet]>
							<cfset pageletName = xmlPagelet.xmlAttributes.name>
							<!--- add the pagelet to the page.pagelets structure if required --->
							<cfif Not structKeyExists(appPage.pagelets, pageletName)><cfset appPage.pagelets[pageletName] = structNew()></cfif>
							<!--- if there is a render order value, then add this otherwise set to zero, also log the error to the request.error array --->
							<cfif structKeyExists(xmlPagelet.xmlAttributes, "order")>
								<cfset iTempOrder = xmlPagelet.xmlAttributes.order>
								<cfif NOT isNumeric(iTempOrder)>
									<cfset instance.rhino.getLog().addEntry("WARNING Although an order was specified for the pagelet, #pageletName# it was not numeric", 2)>
									<cfset iTempOrder = 0>
								</cfif>
							<cfelse>
								<cfset iTempOrder = 0>
							</cfif>
		                    
							<!--- check that a space is defined and then add the pagelet to the structure with a key to represent its place in the order of execution --->
							<cfif NOT structKeyExists(xmlPagelet.xmlAttributes, "space")>
								<cfthrow message="Space attribute is not defined">
							</cfif>
							
							<!--- create a struct for each space defined in this page --->
							<cfif NOT structKeyExists(appPage.space, xmlPagelet.xmlAttributes.space)>
								<cfset appPage.space[xmlPagelet.xmlAttributes.space] = structNew()>
							</cfif>
							<cfset spaceDetails = appPage.space[xmlPagelet.xmlAttributes.space]>
		                    
		                    <!--- each space contains an array of pagelets --->
							<cfif NOT structKeyExists(appPage.space[xmlPagelet.xmlAttributes.space], iTempOrder)>
								<cfset spaceDetails["#iTempOrder#"] = ArrayNew(1)>
							</cfif>
		                    
		                    <!--- each pagelet is defined by a struct [name, scope, type] --->
							<cfset iNextSpaceArrayIndex = arrayLen(spaceDetails["#iTempOrder#"])+1>
							<cfset pageletDetails = StructNew()>
							<cfset pageletDetails.name  = pageletName>
							<cfset pageletDetails.scope = ""> <!--- application, appextension, coreextension - refers to where the pagelet can be found --->
							<cfset pageletDetails.type  = "pagelet"> <!--- pagelet, function --->
							<cfset ArrayAppend(spaceDetails["#iTempOrder#"], pageletDetails)>
						</cfloop>
							
						<!--- process the modules --->
						<!---
						<cfloop from="1" to="#arrayLen(arrConstructModules)#" index="iPageModule">
							<!--- build a list of modules that will be used to render this page --->
							<cfset sPageModule = arrConstructModules[iPageModule].xmlAttributes.name>
							<!--- loop over the pagelets required for the page and check if it exists already in the required pagelets list --->
							<cfif Not structKeyExists(appPage.modules, sPageModule)><cfset appPage.modules[sPageModule] = structNew()></cfif>
							<!--- if there is a render order value, then add this otherwise set to zero --->
							<cfif structKeyExists(arrConstructModules[iPageModule].xmlAttributes, "order")>
								<cfset iTempOrder = arrConstructModules[iPageModule].xmlAttributes.order>
								<cfif NOT isNumeric(iTempOrder)>
									<cfset instance.rhino.getLog().addEntry("WARNING Although an order was specified for the pagelet, #sPagePagelet# it was not numeric", 2)>
									<cfset iTempOrder = 0>
								</cfif>
							<cfelse>
								<cfset iTempOrder = 0>
							</cfif>
							<!--- add the pagelet to the structure with a key to represent its place in the order of execution --->
							<cfif NOT structKeyExists(appPage.space, arrConstructModules[iPageModule].xmlAttributes.space)><cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space] = structNew()></cfif>
							<cfif NOT structKeyExists(appPage.space[arrConstructModules[iPageModule].xmlAttributes.space], iTempOrder)><cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space][iTempOrder] = arrayNew(2)></cfif>
							<cfset iNextSpaceArrayIndex = arrayLen(appPage.space[arrConstructModules[iPageModule].xmlAttributes.space]["#iTempOrder#"])+1>
							<cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space]["#iTempOrder#"][iNextSpaceArrayIndex][1] = sPageModule>
							<cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space]["#iTempOrder#"][iNextSpaceArrayIndex][2] = "module">
						</cfloop> ---><!--- modules loop --->
	
						<!--- finally, save the configuration --->
						<cfset appPages[sFQPageName][constructVersion] = structCopy(appPage)>
						<cfset instance.rhino.getLog().addEntry("Adding page: #sFQPageName#", 4)>

					</cfloop><!--- construct loop --->
				
				<cfelse>
				
					<cfset appPage = structNew()>
					<cfset appPage.layouts = arrayNew(1)>
					<cfset appPage.pagelets = structNew()>
					<cfset appPage.datasets = arrayNew(1)>
					<cfset appPage.modules = structNew()>
					<cfset appPage.params = structNew()>
					<cfset appPage.space = structNew()>
					<cfset appPage.spacePagelets = structNew()>
					<cfset appPage.head = arrayNew(1)>
					<cfset appPage.deleted = True>

					<!--- finally, save the configuration --->
					<cfset appPages[sFQPageName][constructVersion] = structCopy(appPage)>
					<cfset instance.rhino.getLog().addEntry("Adding page: #sFQPageName#", 4)>
				
				</cfif><!--- page deleted --->

			</cfloop><!--- pages loop --->
			
		</cfif>
		<cfset instance.rhino.getLog().addEntry("It took #getTickCount()-startTick#ms to parse and load pages and pagelets", 4)>
		
		<cfreturn appPages>
	</cffunction>
	
	<cffunction name="z__getPages" access="private" returntype="struct" output="false">
		<cfargument name="sCFApp" type="string" required="yes">

		<cfset var appPages = structNew()>
		<cfset var xmlAppPages = xmlParse(instance.xmlfiles.pages)>
		<cfset var namespace = getNamespace(xmlAppPages)>
		<cfset var arrAppPages = arrayNew(1)>
		<cfset var startTick = getTickCount()>
		
		<cfif namespace IS NOT ""><cfset namespace = namespace & ":"></cfif>
		<cfset arrAppPages = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page")>
        
        <cfset instance.rhino.getLog().addEntry("there are #arrayLen(arrAppPages)# pages in #instance.modulename#.")>
		
        <cfif arrayLen(arrAppPages)>
	
			<!--- PAGE parameters --->
			<cfloop index="i" from="1" to="#arrayLen(arrAppPages)#">
				<cfset xmlElement = arrAppPages[i]>
				<cfset appPage = structNew()>
				<cfset appPage.layouts = arrayNew(1)>
				<cfset appPage.pagelets = structNew()>
				<cfset appPage.datasets = arrayNew(1)>
				<cfset appPage.modules = structNew()>
				<cfset appPage.params = structNew()>
				<cfset appPage.space = structNew()>
				<cfset appPage.spacePagelets = structNew()>
				<!--- cfset appPage.construct = structNew() --->
				<cfset appPage.head = arrayNew(1)>
				
				<!--- get the name of the page, also one for the key --->
				<cfset sPageName = xmlElement.xmlAttributes.name>
				<cfif getToken(sPageName, 1, ".") IS NOT sCFApp><cfset sFQPageName = sCFApp & "." & sPageName><cfelse><cfset sFQPageName = sPageName></cfif>
				
                <!--- record any special flag tags --->
				<cfif structKeyExists(xmlElement, "wrap")><cfset appPage.wrap = xmlElement.wrap><cfelse><cfset appPage.wrap = "yes"></cfif>
                <cfif structKeyExists(xmlElement.xmlAttributes, "deleted") AND isBoolean(xmlElement.xmlAttributes.deleted)><cfset appPage.deleted = xmlElement.xmlAttributes.deleted><cfelse><cfset appPage.deleted = false></cfif>
				
				<!--- get the build spec --->
				<cfset arrConstruct = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#instance.rhino.getName()#']/#namespace#page/#namespace#construct")>
				<cfset arrConstructLayouts = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct/#namespace#layout")>
				<cfset arrConstructPagelets = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct/#namespace#pagelet")>
				<cfset arrConstructModules = xmlSearch(xmlAppPages, "/#namespace#cfapp-pages/#namespace#cfapp[@id='#sCFApp#']/#namespace#page[@name='#sPageName#']/#namespace#construct/#namespace#module")>

				<!--- process the layout --->
                <cfif NOT appPage.deleted>
                    <cfif arrayLen(arrConstructLayouts) EQ 0>
                        <cfset instance.rhino.getLog().addEntry("Unable to find any layouts for page #sPageName#")>
                        <cfthrow message="No layout specified" detail="A page must have at least one layout specified. Rhino was looking in the '#sCFApp#' pages.xml for #sPageName# and could find no matching pages.">
                    <cfelse>
                        <cfset arrayAppend(appPage.layouts, arrConstructLayouts[1].xmlAttributes.name)>
                    </cfif>
	
    				<!--- process the pagelets --->
    				<cfif arrayLen(arrConstructPagelets) EQ 0>
        				<cfthrow message="No pagelets found in page #sPageName#">
                    </cfif>
                    
                    <cfloop from="1" to="#arrayLen(arrConstructPagelets)#" index="iPagePagelet">
                        <!--- build a list of required datasets for each page based on the pagelets that are defined for it --->
                        <cfset sPagePagelet = arrConstructPagelets[iPagePagelet].xmlAttributes.name>
                        
                        <!--- add the pagelet to the page.pagelets structure if required --->
                        <cfif Not structKeyExists(appPage.pagelets, sPagePagelet)><cfset appPage.pagelets[sPagePagelet] = structNew()></cfif>
                        
                        <!--- if there is a render order value, then add this otherwise set to zero, also log the error to the request.error array --->
                        <cfif structKeyExists(arrConstructPagelets[iPagePagelet].xmlAttributes, "order")>
                            <cfset iTempOrder = arrConstructPagelets[iPagePagelet].xmlAttributes.order>
                            <cfif NOT isNumeric(iTempOrder)>
                                <cfset instance.rhino.getLog().addEntry("WARNING Although an order was specified for the pagelet, #sPagePagelet# it was not numeric", 2)>
                                <cfset iTempOrder = 0>
                            </cfif>
                        <cfelse>
                            <cfset iTempOrder = 0>
                        </cfif>

					<!--- check that a space is defined and then add the pagelet to the structure with a key to represent its place in the order of execution --->
					<cfif NOT structKeyExists(arrConstructPagelets[iPagePagelet].xmlAttributes, "space")><cfthrow message="Space attribute is not defined"></cfif>
					<cfif NOT structKeyExists(appPage.space, arrConstructPagelets[iPagePagelet].xmlAttributes.space)><cfset appPage.space[arrConstructPagelets[iPagePagelet].xmlAttributes.space] = structNew()></cfif>
					<cfif NOT structKeyExists(appPage.space[arrConstructPagelets[iPagePagelet].xmlAttributes.space], iTempOrder)><cfset appPage.space[arrConstructPagelets[iPagePagelet].xmlAttributes.space]["#iTempOrder#"] = arrayNew(2)></cfif>
					<cfset iNextSpaceArrayIndex = arrayLen(appPage.space[arrConstructPagelets[iPagePagelet].xmlAttributes.space]["#iTempOrder#"])+1>
					<cfset appPage.space[arrConstructPagelets[iPagePagelet].xmlAttributes.space]["#iTempOrder#"][iNextSpaceArrayIndex][1] = sPagePagelet>
				</cfloop>
                </cfif>
					
				<!--- process the modules --->
				<cfloop from="1" to="#arrayLen(arrConstructModules)#" index="iPageModule">
					<!--- build a list of modules that will be used to render this page --->
					<cfset sPageModule = arrConstructModules[iPageModule].xmlAttributes.name>
					<!--- loop over the pagelets required for the page and check if it exists already in the required pagelets list --->
					<cfif Not structKeyExists(appPage.modules, sPageModule)><cfset appPage.modules[sPageModule] = structNew()></cfif>
					<!--- if there is a render order value, then add this otherwise set to zero --->
					<cfif structKeyExists(arrConstructModules[iPageModule].xmlAttributes, "order")>
						<cfset iTempOrder = arrConstructModules[iPageModule].xmlAttributes.order>
						<cfif NOT isNumeric(iTempOrder)>
							<cfset instance.rhino.getLog().addEntry("WARNING Although an order was specified for the pagelet, #sPagePagelet# it was not numeric", 2)>
							<cfset iTempOrder = 0>
						</cfif>
					<cfelse>
						<cfset iTempOrder = 0>
					</cfif>
					<!--- add the pagelet to the structure with a key to represent its place in the order of execution --->
					<cfif NOT structKeyExists(appPage.space, arrConstructModules[iPageModule].xmlAttributes.space)><cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space] = structNew()></cfif>
					<cfif NOT structKeyExists(appPage.space[arrConstructModules[iPageModule].xmlAttributes.space], iTempOrder)><cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space][iTempOrder] = arrayNew(2)></cfif>
					<cfset iNextSpaceArrayIndex = arrayLen(appPage.space[arrConstructModules[iPageModule].xmlAttributes.space]["#iTempOrder#"])+1>
					<cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space]["#iTempOrder#"][iNextSpaceArrayIndex][1] = sPageModule>
					<cfset appPage.space[arrConstructModules[iPageModule].xmlAttributes.space]["#iTempOrder#"][iNextSpaceArrayIndex][2] = "module">
				</cfloop>
					
				<cfset appPages[sFQPageName] = structCopy(appPage)>
                <cfset instance.rhino.getLog().addEntry("Adding page: #sFQPageName#", 4)>
			</cfloop>
		</cfif>
		<cfset instance.rhino.getLog().addEntry("It took #getTickCount()-startTick#ms to parse and load pages and pagelets", 4)>
		
		<cfreturn appPages>
	</cffunction>
	
	<cffunction name="getControllers" access="private" returntype="struct" output="false">
		<cfargument name="sCFApp" type="string" required="yes">
		<cfset appControllers = structNew()>
		<cfset xmlAppControllers = xmlParse(instance.xmlfiles.controllers)>
		<cfset this.namespace = getNamespace(xmlAppControllers)>
		<cfset arrAppControllers = xmlSearch(xmlAppControllers, "/#this.namespace#:cfapp-controllers/#this.namespace#:controller")>
		<cfloop from="1" to="#arrayLen(arrAppControllers)#" index="iController">
			<cfset appControllers[arrAppControllers[iController].xmlAttributes.alias] = arrAppControllers[iController].xmlAttributes.name>
		</cfloop>
		<cfreturn appControllers>
	</cffunction>
	
	<cffunction name="getNamespace" access="private" returntype="string" output="false">
		<cfargument name="xmlDoc" type="string" required="yes">
		<cfset xmlNameSpace = xmlSearch(xmlDoc, "/*")>
		<cfreturn xmlNameSpace[1].XmlNsPrefix>
	</cffunction>

	<!--- the preceeding functions take the xml files and parse them into a set of basic data, the following functions
	manipulate this basic data to form exactly which pagelets are required for a page, data for pagelets and parameters
	for data. This is a separate task, since we must have all the application and module xml files parsed before we can
	do this part --->
	<cffunction name="processPages" access="private" returntype="struct" output="false">
        <cfargument name="strPages" type="struct" required="true">
		
		<cfset var iPage = 1>
		<cfset var sPageName = "">
		<cfset var arrAllPages = ArrayNew(1)>
		<cfset var allConstructs = ArrayNew(1)>
		<cfset var page = StructNew()>
	
		<!--- firstly, get every page. We will loop over this array checking and assembing data for each page --->
		<cfset arrAllPages = structKeyArray(arguments.strPages)>
		<cfloop from="1" to="#arrayLen(arrAllPages)#" index="iPage">

			<cfset sPageName = arrAllPages[iPage]>

			<!--- get all the constructs and process each one --->
			<cfset allConstructs = StructKeyArray(strPages[sPageName])>
			<cfloop array="#allConstructs#" index="construct">
				
				<cfset page = strPages[sPageName][construct]>
				
				<!--- get all pagelets that the page will use --->
				<cfset arrAllPagePagelets = StructKeyArray(page.pagelets)>
				
				<!--- now generate the pagelet sequence for each space in the page --->
				<cfscript>
				arrSpaces = structKeyArray(page.space);
				for (iSpace = 1; iSpace LTE arrayLen(arrSpaces); iSpace=iSpace+1) {
					strSpacePagelets = page.space[arrSpaces[iSpace]];
					arrPageletOrder = structKeyArray(strSpacePagelets);
					arraySort(arrPageletOrder, "numeric", "ASC");
					// loop over the sorted array, creating a new array of pagelets in execution order
					fZero = false;
					loc = 1;
					arrSpacePagelets = arrayNew(1);
					for (iSpacePagelet = 1; iSpacePagelet LTE arrayLen(arrPageletOrder); iSpacePagelet = iSpacePagelet + 1) {
						if (loc EQ 1 AND arrPageletOrder[loc] EQ 0) {
							loc = loc + 1;
							fZero = true;
						} else {
							// add the pagelets at this order to the arrSpacePagelets
							for (iOrderPagelet = 1; iOrderPagelet LTE arrayLen(strSpacePagelets[arrPageletOrder[loc]]); iOrderPagelet = iOrderPagelet + 1) {
								 arrayAppend(arrSpacePagelets, strSpacePagelets[arrPageletOrder[loc]][iOrderPagelet]);
							}
							loc = loc + 1;
						}
					}
					// now if it found zero order pagelets, add these last
					if (fZero) {
						for (iOrderPagelet = 1; iOrderPagelet LTE arrayLen(strSpacePagelets[0]); iOrderPagelet = iOrderPagelet + 1) {
							 arrayAppend(arrSpacePagelets, strSpacePagelets[0][iOrderPagelet]);
						}
					}
					// finally assign the array to the appPage
					page.spacePagelets[arrSpaces[iSpace]] = arrSpacePagelets;
				}
				</cfscript>
			</cfloop><!--- constructs --->
		</cfloop><!--- pages --->
		
        <cfreturn strPages>
	</cffunction>
	
	<!--- the preceeding functions take the xml files and parse them into a set of basic data, the following functions
	manipulate this basic data to form exactly which pagelets are required for a page, data for pagelets and parameters
	for data. This is a separate task, since we must have all the application and module xml files parsed before we can
	do this part --->
	<cffunction name="z__processPages" access="private" returntype="struct">
        <cfargument name="strPages" type="struct" required="true">
	
		<!--- firstly, get every page. We will loop over this array checking and assembing data for each page --->
		<cfset arrAllPages = structKeyArray(arguments.strPages)>
		<cfloop from="1" to="#arrayLen(arrAllPages)#" index="iPage">
			<!--- get all pagelets that the page will use --->
			<cfset sPageName = arrAllPages[iPage]>
			<cfset arrAllPagePagelets = structKeyArray(arguments.strPages[sPageName].pagelets)>
			<!--- now generate the pagelet sequence for each space in the page --->
			<cfscript>
			arrSpaces = structKeyArray(arguments.strPages[sPageName].space);
			for (iSpace = 1; iSpace LTE arrayLen(arrSpaces); iSpace=iSpace+1) {
				strSpacePagelets = arguments.strPages[sPageName].space[arrSpaces[iSpace]];
				arrPageletOrder = structKeyArray(strSpacePagelets);
				arraySort(arrPageletOrder, "numeric", "ASC");
				// loop over the sorted array, creating a new array of pagelets in execution order
				fZero = false;
				loc = 1;
				arrSpacePagelets = arrayNew(1);
				for (iSpacePagelet = 1; iSpacePagelet LTE arrayLen(arrPageletOrder); iSpacePagelet = iSpacePagelet + 1) {
					if (loc EQ 1 AND arrPageletOrder[loc] EQ 0) {
						loc = loc + 1;
						fZero = true;
					} else {
						// add the pagelets at this order to the arrSpacePagelets
						for (iOrderPagelet = 1; iOrderPagelet LTE arrayLen(strSpacePagelets[arrPageletOrder[loc]]); iOrderPagelet = iOrderPagelet + 1) {
							 arrayAppend(arrSpacePagelets, strSpacePagelets[arrPageletOrder[loc]][iOrderPagelet]);
						}
						loc = loc + 1;
					}
				}
				// now if it found zero order pagelets, add these last
				if (fZero) {
					for (iOrderPagelet = 1; iOrderPagelet LTE arrayLen(strSpacePagelets[0]); iOrderPagelet = iOrderPagelet + 1) {
						 arrayAppend(arrSpacePagelets, strSpacePagelets[0][iOrderPagelet]);
					}
				}
				// finally assign the array to the appPage
				arguments.strPages[sPageName].spacePagelets[arrSpaces[iSpace]] = arrSpacePagelets;
			}
			</cfscript>
		</cfloop>
        <cfreturn strPages>
	</cffunction>

	<cffunction name="getWiring" access="private" returntype="struct" output="false">
		<cfargument name="sCFApp" type="string" required="yes">
		
		<cfset var wiredBeans = structNew()>
		<cfset var wiredBean = "">
		<cfset var wiredBeanAttributes = structNew()>
		<cfset var iBean = 0>
		<cfset var wiredBeansArray = ArrayNew(1)>
		
		<!--- parse the locals file for the setting of instance relevant only to the current app --->
		<cfset var xmlWiring = xmlParse(instance.xmlfiles.wires)>
		<cfset var namespace = getNamespace(xmlWiring)>
		
		<cfif namespace IS NOT ""><cfset namespace = "#namespace#:"></cfif>

		<cfset wiredBeansArray = xmlSearch(xmlWiring, "/#namespace#cfapp-wires/#namespace#cfapp[@id='#sCFApp#']/#namespace#wiredbean")>
		
		<cfif arrayLen(wiredBeansArray)>
		
			<cfloop from="1" to="#arrayLen(wiredBeansArray)#" index="iBean">
				<cfset wiredBean = wiredBeansArray[iBean]>
				
				<cfset wiredBeanAttributes = wiredBean.xmlAttributes>
			
				<!--- provide support for the new format of XML --->
				<cfif NOT structKeyExists(wiredBeans, wiredBeanAttributes.name)>
					
					<!--- Virtual path parameters --->
					<cfset wiredBeans[wiredBeanAttributes.name] = structNew()>
					<cfset wiredBeans[wiredBeanAttributes.name].class = wiredBeanAttributes.class>
					
				</cfif>
			</cfloop>
			
		<cfelse>
			<cfset instance.rhino.getLog().addEntry("No wire definitions found for '#arguments.sCFapp#'", 3)>

		</cfif>
		
		<cfreturn wiredBeans>
	</cffunction>
		
</cfcomponent>
