<cfcomponent hint="manages loading modules for the current application">
	
	<cfset instance = structNew()>
	<cfset instance.extensions = structNew()>

	<cffunction name="init" access="public" output="false">
		<!--- first we load the extension event handlers --->
		<cfset this.readExtensions()>
	</cffunction>
    
	
	<cffunction name="getExtensionList" access="public" returntype="string">
		<cfreturn arrayToList(structKeyArray(instance.extensions))>
	</cffunction>
	
	<cffunction name="getExtensions" access="public" returntype="struct">
		<cfreturn instance.extensions>
	</cffunction>

	<cffunction name="getCurrentExtension" access="public" returntype="string">
		<cfreturn instance.currentExtension>
	</cffunction>
	
	<cffunction name="extensionExists" access="public" returntype="boolean">
		<cfargument name="sExtensionName" type="string">
		<cfif structKeyExists(instance.extensions, sExtensionName)><cfreturn true><cfelse><cfreturn false></cfif>
	</cffunction>

</cfcomponent>