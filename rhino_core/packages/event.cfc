<cfcomponent>
	
	<!---//
    The Rhino event object adds to the Rhino framework the ability to handle request level
    events. This allows the framework to handle a variety of framework and user defined
    circumstances. One common event that makes Rhino particularly flexible is the
    onPageletUndefined, which allows the framework to handle requests that have not been
    explicitly defined in the config.	
	--->
    
    <cfscript>
        instance.number = 0;
        instance.arrNames = arrayNew(1);
        instance.events = structNew();
        instance.extensions = structNew();
        instance.enum = structNew();
    </cfscript>
	
	<!--- initialisation of the event object requires a rhino object to be passed in --->
	<cffunction name="init" access="public" output="false">
        <cfargument name="refRhino" type="rhino" required="true">
		<cfscript>
            instance.rhino = arguments.refRhino;
		</cfscript>
        <cfreturn this>
	</cffunction>
	
	<!--- Provides a mechanism to parse application and extension cfc's that provide event extension
	points. Takes either a 'application','appextension','coreextension' --->
	<cffunction name="readExtensions" access="public" output="true">
        <cfargument name="extType" type="string" required="true">
        
        <cfset var compTemp = {}>
        <cfset var extRootDir = "">
        <cfset var extDotRoot = "">
        <cfset var dirExtensions = queryNew("")>

        <!--- establish the file locations for extensions --->
        <cfif arguments.extType IS "app">
            <cfset extRootDir = instance.rhino.roots().applicationRootPath & "extensions/">
            <cfset extDotRoot = instance.rhino.roots().applicationDotRoot & ".extensions">
        <cfelseif arguments.extType IS "core">
            <cfset extRootDir = instance.rhino.roots().rhinoRootPath & "extensions/">
            <cfset extDotRoot = instance.rhino.roots().rhinoDotRoot & ".extensions">
        <cfelse>
            <cfthrow message="Object scope specified was not one of either 'app' or 'core'"
                detail="Extensions within CFRhino can only live in the application's extension folder or the core extension folder.">
        </cfif>

        <cfset instance.rhino.getLog().addEntry("[Event] Reading in extensions from #extRootDir#")>
        
        <!--- this looks in the extensions directory of the framework (rhino_core) and loads any extensions that are there --->
		<cfdirectory action="list" directory="#extRootDir#" name="dirExtensions">
        
        <cfset instance.rhino.getLog().addEntry("[Event] #dirExtensions.recordCount# extensions found in #extRootDir#")>
        
		<cfloop query="dirExtensions" endrow="10">
            
			<!--- we should only import the extension if allowed to, this is obtained from instance.rhino.base.moduleRequired(), which will return a 0
			if the module should not be loaded, a 1 if it should be loaded and a 2 if it MUST be loaded. --->
			<cfif (instance.rhino.moduleRequired(name) EQ 1 OR instance.rhino.moduleRequired(name) EQ 2) AND type IS "dir">
                
                <cfset instance.rhino.getLog().addEntry("[Event] Loading extension #name#")>
				
                <cftry>
					<cfscript>
						// create the extension and initialise it by calling the init() method if it exists
						compTemp = createObject("component", "#extDotRoot#.#name#.#name#");
						if (structKeyExists(compTemp, "init")) compTemp.init();
					</cfscript>
                    
                    <!--- pass the extension to cacheExtension where public methods are cached --->
                    <cfset this.cacheExtension(compTemp, "", "#arguments.extType#extension")>
    
					<!--- read and cache the configuration --->
					<cfif directoryExists("#extRootDir##name#/config")>
			            <cfset instance.rhino.getLog().addEntry("[Event] Loading configuration xml files for #name#")>
						<cfset strModuleConfig = instance.rhino.getParser().loadConfig(name, "#arguments.extType#extension")>
			            <cfset instance.rhino.saveParsed(strModuleConfig)>
			        <cfelse>
			            <cfset instance.rhino.getLog().addEntry("No configuration xml files found for #name#", 3)>
					</cfif>
		            <cfset instance.rhino.getLog().addEntry("[Event] Loaded configuration for #name#")>
		        
					<cfcatch>
                        <cfset instance.rhino.getLog().addEntry("[Event] Error loading configuration for #name#: #cfcatch.message#/#cfcatch.detail#", 1)>
                        <cfif instance.rhino.moduleRequired(name) EQ 2><cfthrow message="Required extension, #name#, could not be found or was invalid" detail="#cfcatch.message#"></cfif>
                    </cfcatch>
				</cftry>
                
			<cfelse>
				<cfset instance.rhino.getLog().addEntry("[Event] Ignoring #name#; Not a directory or no root cfc found; #instance.rhino.moduleRequired(name)#", 4)>
			</cfif>

		</cfloop>
		
	</cffunction>
    
    <!--- Allows the specification of an extension, which gets read and cached --->
    <cffunction name="cacheExtension" access="public" returntype="string" output="false">
        <cfargument name="objExtension" type="any">
        <cfargument name="sName" type="string" required="false" default="">
        <cfargument name="extensionScope" type="string" required="false" default="">
        
        <cfset var metaData = {}>
        <cfset var arrMethods = arrayNew(1)>
        <cfset var iFunc = 0>
        <cfset var fReturnType = false>
        
        <!--- if the passed in objExtension argument is not an object, then error --->
		<cfif NOT isObject(objExtension)>
            <cfset instance.rhino.getLog().setEntry("[Event] Argument passed in to cacheExtension is not of type object")>
        </cfif>
        
        <!--- get the CFC meta data struct --->
        <cfset metaData = getMetaData(objExtension)>
        
        <!--- if no name is specified we create it from the components name --->
        <cfif sName IS ""><cfset sName = listGetAt(metaData.name, listLen(metaData.name, "."), ".")></cfif>
        
        <!--- attempt to get all the functions from the cfc for registering with the event framework --->
        <cftry>
            <cfset arrMethods = metaData.functions>
            <cfcatch><cfthrow message="Unable to determine meta data on the applications main cfc"
                detail="The applications cfc does not seem to contain any functions. Please check that it is a valid ColdFusion component #cfcatch.message#/#cfcatch.detail#"></cfcatch>
        </cftry>
		
        <!--- register each public method of the cfc with the event framework --->
		<cfset instance.rhino.getLog().addEntry("[Event] Checking for Public functions on #sName#", 4)>
		<cfloop from="1" to="#arrayLen(arrMethods)#" index="iFunc">
			<cfif NOT structKeyExists(arrMethods[iFunc], "access") OR arrMethods[iFunc].access IS "" OR lcase(arrMethods[iFunc].access) IS "public">
				<!--- Check if a returntype is defined this will aid calling extensions. --->
				<cfif StructKeyExists(arrMethods[iFunc], "returntype") AND arrMethods[iFunc].returntype is not "void">
					<cfset fReturnType = true>
				<cfelse>
					<cfset fReturnType = false>
				</cfif>
				<cfset instance.rhino.getLog().addEntry("[Event] Adding function: #arrMethods[iFunc].name#", 4)>
				<cfset this.setHandler(sName, arrMethods[iFunc].name, fReturnType)>
				<!--- if the component is not already cached, then cache it --->
				<cfif Not structKeyExists(instance.extensions, sName)>
                    <cfset instance.extensions[sName] = structNew()>
                    <cfset instance.extensions[sName].base = objExtension>
                    <cfset instance.extensions[sName].scope = arguments.extensionScope>
                </cfif>
			</cfif>
		</cfloop>
        
        <!--- return the name we used to cache it --->
        <cfreturn sName>
    </cffunction>
    
    <!--- Provides a means to obtain an extension that has been cached. --->
    <cffunction name="getExtension" access="public" returntype="any" output="false">
        <cfargument name="extensionName" type="string" required="true">
        <cfif structKeyExists(instance.extensions, arguments.extensionName)>
            <cfreturn instance.extensions[arguments.extensionName].base>
        <cfelse>
            <cfreturn "">
        </cfif>
    </cffunction>
    
	<!--- register methods for events --->
	<cffunction name="setHandler" access="public">
		<cfargument name="sExtension" type="string" required="yes">
		<cfargument name="sMethod" type="string" required="yes">
		<cfargument name="fReturnType" type="boolean" required="no" default="false">
        
        <cfset instance.rhino.getLog().addEntry("[Event] Adding #sExtension#.#sMethod#()")>
        
        <!--- add the public methods --->
		<cfif structKeyExists(instance.events, arguments.sMethod)>
			<cfset instance.events[arguments.sMethod][arguments.sExtension] = arguments.fReturnType>
		<cfelse>
			<cfset instance.events[arguments.sMethod] = StructNew()>
			<cfset instance.events[arguments.sMethod][arguments.sExtension] = arguments.fReturnType>
		</cfif>
	</cffunction>
	
	
	<!--- pass in an event name and call the method on extensions whose implementations support it --->
	<cffunction name="callHandler" access="public" returntype="any" output="false">
		<cfargument name="sEvent" required="yes" type="string">
		<cfargument name="sExtension" required="no" type="string" default="" hint="define which extensions you wish to handle this event">
		<cfargument name="fSingle" required="no" type="boolean" default="false" hint="if true, stop processing other extensions as soon as it's been handled once">
        
        <cfset var currentExtension = "">
        <cfset var keyExtension = "">
        <cfset var whatsreturned = "">
        <cfset var attributeCollection = StructNew()>
        
        <cfset var io = structNew()>
        <cfset io.set = false>
        <cfset io.data = whatsreturned>
        
        <!--- log the extension call --->
        <cfset instance.rhino.getLog().addEntry("An event, #arguments.sEvent#, was called. (sExtension=#arguments.sExtension#, fSingle=#arguments.fSingle#)")>
        <!--- if the sExtension argument is passed then check that the chosen extension exists and call only it's method --->
		<cfif structKeyExists(instance.events, arguments.sEvent)>
			<cfloop collection="#instance.events[arguments.sEvent]#" item="keyExtension">
				<!--- you can proceed if:
					only one extension should be fired but this is the first
				OR	an extension is specified and this is the one
				OR	no specification made, so process them all
				--->
                <cfset currentExtension = keyExtension>
                <cfif (arguments.sExtension IS "" OR arguments.sExtension IS currentExtension) AND NOT (arguments.fSingle AND io.set)>
					<!--- Build the attributeCollection to be used by the cfinvoke --->
					<cfset attributeCollection["component"] = instance.extensions[currentExtension].base>
					<cfset attributeCollection["method"] = arguments.sEvent>
                    
					<!--- If the extension has a returntype defined --->
					<cfif instance.events[arguments.sEvent][keyExtension]>
						<cfset attributeCollection["returnvariable"] = "whatsreturned">
					</cfif>
                    
					<!--- load any other arguments into the argument collection, that the handler might want --->
                    <cfloop collection="#arguments#" item="argName">
                        <cfif NOT ListFindNoCase("component,method,returnvariable,fsingle", argName)>
                            <cfset attributeCollection[argName] = arguments[argName]>
                        </cfif>
                    </cfloop>
                    <cfset instance.rhino.getLog().addEntry("Checking if extension #currentExtension# can handle #arguments.sEvent#")>
					<cfinvoke component="#instance.extensions[currentExtension].base#" method="#arguments.sEvent#" returnvariable="whatsreturned">
                    <!--- this works in Railo, but not CF
                    <cfinvoke attributeCollection="#attributeCollection#">
					--->
                    
                    <cfif IsDefined("whatsreturned")><cfset io.data = whatsreturned></cfif>
					<cfset io.handler = currentExtension>
					<cfset io.event = arguments.sEvent>
					<cfset io.set = true>
					<cfset instance.rhino.getLog().addEntry("Extension #currentExtension# can handle #arguments.sEvent#")>
				
                <cfelse>
                    <cfset instance.rhino.getLog().addEntry("Extension #currentExtension# is not called since it is either not the specified extension (#arguments.sExtension#) or another extension has already responded")>
            	</cfif>
			</cfloop>
        <cfelse>
            <cfset instance.rhino.getLog().addEntry("No extension able to handle event: #arguments.sEvent#",4)>
		</cfif>
        
        <cfreturn io>
        
	</cffunction>
	
	
	<!--- helper functions --->
	<cffunction name="getConfig" access="public">
		<cfargument name="sConfigLocation" type="string">
	</cffunction>
	
    <!--- getInterceptAllowed checks whether the given extension is allowed to
    intercept layouts. It will return a boolean to that effect --->
	<cffunction name="getInterceptAllowed" access="public" returntype="boolean">
		<cfargument name="sModuleName" type="string">
        
        <cfset var iExtension = 0>
        <cfset var interceptors = ArrayNew(1)>
        
		<cfif structKeyExists(instance.events, "onRequestPageUndefined") AND
				StructKeyExists(instance.events["onRequestPageUndefined"], sModuleName)>
			<cfreturn true>
		<!---
		Removed this as if an applications cfc defines onRequestPageUndefined then it implicitly handles intercepts
		<cfelseif structKeyExists(instance.events, "interceptLayout")>
        	<cfset interceptors = StructKeyArray(instance.events.interceptlayout)>
			<cfloop from="1" to="#arrayLen(interceptors)#" index="iExtension">
				<cfif interceptors[iExtension] IS arguments.sModuleName>
                    <cfreturn true>
                </cfif>
			</cfloop>
		--->
		</cfif>
		<cfreturn false>
	</cffunction>
    
    <!--- getLoadedExtensions returns an array of extension names --->
    <cffunction name="getLoadedExtensions" access="public" output="false">
        <cfset arrReturn = structKeyArray(instance.extensions)>
        <cfreturn instance.extensions>
    </cffunction>
    <!--- isLoadedExtension returns true/false if an extension is loaded --->
    <cffunction name="isLoadedExtension" access="public" output="false">
        <cfargument name="sExtension" type="string">
        <cfreturn structKeyExists(instance.extensions, arguments.sExtension)>
    </cffunction>
    <!--- returns the scope of an extension that has been loaded. If no match is made to
    a loaded component, then an empty string is returned. --->
    <cffunction name="getExtensionScope" access="public" output="false">
        <cfargument name="sExtension" type="string">
        <cfif structKeyExists(instance.extensions, arguments.sExtension)>
            <cfreturn instance.extensions[arguments.sExtension].scope>
        <cfelse>
            <cfreturn "">
        </cfif>
    </cffunction>
    <!--- Return a list of all the events that are currently registered --->
    <cffunction name="getHandledEvents" access="public" output="false">
        <cfreturn instance.events>
    </cffunction>
    
    
    <!---
    These methods should be called by the caller of the event, this will prevent threading issues
    around this object when the framework gets busy. Remove these once they are no longer required
    <!--- process some event io data --->
    <cffunction name="hasIO" access="public" returntype="boolean" output="false">
        <cfreturn instance.io.isSet()>
    </cffunction>
    
    <cffunction name="getIOData" access="public" returntype="any" output="false">
        <cfreturn instance.io.getData()>
    </cffunction>
    
    <cffunction name="getIOHandler" access="public" returntype="string" output="false">
        <cfreturn instance.io.getHandler()>
    </cffunction>
    --->
	
</cfcomponent>
