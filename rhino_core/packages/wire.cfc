<cfcomponent hint="performs CFRhino based auto-wiring" output="false">
	
	<cfset variables.objectCache = structNew()>
	<cfset variables.spacer = 0>
    <cfset variables.callInit = true>
	
	<cffunction name="init" output="false">
		<cfargument name="rhinoComponent" required="true">
	    <cfargument name="attributes" type="struct" required="true">

		<cfset variables.rhinoCore = arguments.rhinoComponent>
		<cfset variables.attributes = arguments.attributes>
		
		<cfreturn this>
	</cffunction>
	
	<cffunction name="getCache" output="false">
		<cfreturn variables.objectCache>
	</cffunction>
	
	<cffunction name="wireObject" access="public" returntype="any" output="false">
		<cfargument name="baseComponentName" required="true">
		<cfargument name="useCache" required="false" default="true">
        <cfargument name="callInit" required="false" type="boolean" default="true">
        
		<cfset var baseComponent = "">
		<cfset var localDependents = structNew()>
		<cfset var dependentNameArray = arrayNew(1)>
		<cfset var dependentCounter = 0>
		<cfset var iDependent = 0>
		<cfset var dependentCache = structNew()>
		<cfset var tempObject = {}>
		<cfset var componentName = "">
		<cfset var spaces = "">
        
        <cftry>
            <cfset baseComponent = request.data.getObject(objectName = baseComponentName, cache=arguments.useCache)>
            <cfcatch><cfrethrow><cfabort></cfcatch>
        </cftry>
		
		<cfset variables.spacer = variables.spacer + 1>
        <cfset variables.callInit = arguments.callInit>
		
		<cfoutput>#repeatString("--", variables.spacer)#Wiring #baseComponentName#</cfoutput><br/>
		
		<cfset variables.objectCache[baseComponentName] = structNew()>
		<cfset variables.objectCache[baseComponentName].complete = false>
		
		<cfif isDefined("baseComponent.dependents")>
		
			<cfset localDependents = baseComponent.dependents>
			<cfset dependentNameArray = structKeyArray(localDependents)>
			<cfset dependentCounter = arrayLen(dependentNameArray)>
			
			<!--- recurse over dependencies --->
			<cfif dependentCounter GT 0>
				<cfloop from="1" to="#dependentCounter#" index="iDependent">
					<cfoutput>#repeatString("--", variables.spacer)#DependentCounter: #iDependent# / #dependentCounter#</cfoutput><br/>
					<cftry>
						<cfset componentName = localDependents[dependentNameArray[iDependent]].class>
						<cfcatch>
							<cfdump var="#dependentNameArray#">
							<cfdump var="#cfcatch#">
							<cfoutput>#repeatString("--", variables.spacer)#DependentCounter: #dependentCounter#</cfoutput><br/>
							
							<cfabort>
						</cfcatch>
					</cftry>
					
					<!--- if we've already requested the object and it's not yet completed, we throw an error as this
					implies a circular dependency that we can't satisfy --->
					<cfif structKeyExists(variables.objectCache, componentName) AND NOT variables.objectCache[componentName].complete>
						<cfoutput>#repeatString("--", variables.spacer)##componentName# appears to be circularly dependent</cfoutput>
						<cfthrow message="Circular dependency detected"
								detail="#baseComponentName# include a reference to #componentName# which is unsatisfiable.">
					
					<!--- otherwise, if the object has already been instantiated and is complete then just
					return it --->
					<cfelseif structKeyExists(variables.objectCache, componentName)>
						<cfoutput>#repeatString("--", variables.spacer)##componentName#</cfoutput> already Instantiated<br/>
						<cfset dependentCache[componentName] = variables.objectCache[componentName].component>
					
					<!--- finally, if we've not seen this object before, we instantiate it. We do
					this by recursion.  --->
					<cfelse>
						<cfoutput>#repeatString("--", variables.spacer)#Creating #componentName#</cfoutput><br/>
						<cfset tempObject = this.wireObject(componentName, useCache, callInit)>
						<cfoutput>#repeatString("--", variables.spacer)# #componentName# Instantiated</cfoutput><br/>
		
						<cfset tempObject = finishWiring(tempObject)>
						<cfset variables.objectCache[componentName].complete = true>
						<cfset variables.objectCache[componentName].component = tempObject>
						<cfset dependentCache[componentName] = variables.objectCache[componentName].component>
						
					</cfif>
				</cfloop>
			</cfif>
			
			<!--- finally inject the instantiated 'wired' beans into the parent --->
			<cfset baseComponent.wireup(dependentCache)>
		<cfelse>
			<cfoutput>#repeatString("--", variables.spacer)#No dependencies in #baseComponentName#</cfoutput><br/>
		</cfif>
		
		<cfset variables.spacer = variables.spacer - 1>
		<cfset baseComponent = finishWiring(baseComponent)>
		<cfset variables.objectCache[baseComponentName].complete = true>
		<cfset variables.objectCache[baseComponentName].component = baseComponent>
		
		<cfreturn baseComponent>
	</cffunction>
	
    <!--- Finalises the wiring of the chosen component.
    NOTE:   If you allow the wiring process to call init() on your top level component AND that call to init()
            returns a value, then that is what will get returned. NOT the component on which the init() was called
    --->
	<cffunction name="finishWiring" access="private" output="false">
		<cfargument name="objectReference" required="true">
		
		<cfset arguments.tempObject = "">
		
		<!--- set the variables.core/attributes if available --->
		<cfif structKeyExists(objectReference, "setAttributes")>
			<cfoutput>#repeatString("--", variables.spacer)# Setting attributes</cfoutput><br/>
			<cfset objectReference.setAttributes(variables.attributes)>
		</cfif>
		
		<!--- call the implicit init method --->
		<cfif structKeyExists(objectReference, "init") AND variables.callInit>
			<cfoutput>#repeatString("--", variables.spacer)# calling init</cfoutput><br/>
			<cfset arguments.tempObject = objectReference.init()>
			<!--- if the call to init returns an object, assume its of the same type and should overwrite the old refernce --->
			<cfif structKeyExists(arguments, "tempObject") AND 
					isObject(arguments.tempObject)>
						<cfset arguments.objectReference = arguments.tempObject>
			</cfif>
		</cfif>
		
		<cfreturn arguments.objectReference>
	</cffunction>
	
</cfcomponent>