<cfcomponent>
    
    <cfscript>
        instance = structNew();
	    instance.log = arrayNew(1);
	    arrayResize(instance.log, 500);
	    arraySet(instance.log,1,500,"");
	    instance.logPointer = 1;
	    instance.sev = arrayNew(1);
	    instance.sev[1] = "Error: ";
	    instance.sev[2] = "Warning: ";
	    instance.sev[3] = "Info: ";
	    instance.sev[4] = "Debug: ";
	    instance.defaultlevel = 3;
	    instance.min_level = 4;
	    instance.roro = false;
	    instance.log_enable = false;
    </cfscript>

	<cffunction name="init" access="public" output="false">
        <cfargument name="refRhino" type="rhino" required="true">
		<cfargument name="minLevel" type="numeric" required="false">
		
        <cfset instance.rhino = arguments.refRhino>
		
		<cfif structKeyExists(arguments, "minLevel")>
			<cfset instance.min_level = arguments.minLevel>
		</cfif>
		
        <cfreturn this>
	</cffunction>

    <cffunction name="addEntry" access="public" returntype="boolean" output="false">
        <cfargument name="sEntry" type="string" required="true">
        <cfargument name="iSeverity" type="numeric" required="false" default="#instance.defaultlevel#">
        
        <cfif arguments.iSeverity GT instance.min_level>
            <cfreturn false>
        </cfif>
        
        <!--- we assume (!) that rhino_log is defined in rhino.cfc's config and
        if it's not enabled, we return without any logging function --->
        <cfif NOT instance.log_enable>
            <cfreturn false>
        </cfif>
        
        <!--- check that iSeverity is in bounds, otherwise use default --->
        <cfif arguments.iSeverity GT arrayLen(instance.sev) OR arguments.iSeverity LT 1>
            <cfset arguments.iSeverity = instance.defaultlevel>
        </cfif>
        
        <!--- add the log message to the in memory struct --->
        <cflock name="logAccess" type="exclusive" timeout="1" throwontimeout="false">
            <cfset instance.log[instance.logPointer] = "#dateformat(now(), "MM/DD/YYYY")# #timeformat(now(), "HH:MM:SS")# #instance.sev[iSeverity]# #sEntry#">
            <cfset instance.logPointer = instance.logPointer + 1>
            <cfif instance.logPointer GT 500>
				<cfset instance.logPointer = 1>
				<cfset instance.roro = true>
			</cfif>
        </cflock>
        
        <cfreturn true>
    </cffunction>
    
    <cffunction name="getLog" access="public" output="false">
        <cfset var tempLog = arrayNew(1)>
		<cfset var tempLogSize = 500>
        <cfset var iTemp = 0>
		
		<cfif NOT instance.roro><cfset tempLogSize = instance.logPointer></cfif>
		<cfset arrayResize(tempLog, tempLogSize)>
	    
        <cflock name="logAccess" type="readonly" timeout="5" throwontimeout="true">
			<cfif instance.roro>
	            <cfloop from="#instance.logPointer#" to="500" index="iTemp">
	                <cfset tempLog[iTemp-instance.logPointer+1] = "end" & instance.log[iTemp]>
	            </cfloop>
	        </cfif>
            <cfloop from="1" to="#instance.logPointer-1#" index="iTemp">
                <cfset tempLog[iTemp+tempLogSize-instance.logPointer+1] = "start" & instance.log[iTemp]>
            </cfloop>
        </cflock>

		<cfreturn tempLog>
    </cffunction>
    
    <!--- sets the internal value for whether logging is enabled --->
    <cffunction name="EnableLog" access="package" output="false">
        <cfargument name="enabled">
        <cfif isBoolean(arguments.enabled) AND arguments.enabled>
            <cfset instance.log_enable = true>
        </cfif>
    </cffunction>

</cfcomponent>