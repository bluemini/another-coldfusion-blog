<cfcomponent output="false">

	<cfset instance = structNew()>
    <cfset attributes = structNew()>
	
	<cffunction name="init" access="public" output="false">
	    <cfargument name="refRhino" type="rhino" required="true">
	    <cfargument name="refVariables" type="struct" required="false">
	    
	    <cfif structKeyExists(arguments, "refVariables")>
            <cfset attributes = arguments.refVariables>
		</cfif>
	    
	    <cfset instance.rhino = arguments.refRhino>
	    <cfreturn this>
	</cffunction>
	
	<cffunction name="getAsset" access="public" returntype="string" output="false">
		<cfargument name="sAssetName" type="string" required="yes">
		<cfargument name="sAssetType" type="string" required="no" default="application">
		
		<cfset var assetOutput = getAssetContent(arguments.sAssetName, arguments.sAssetType)>
        
		<cfreturn trim(assetOutput)>
	</cffunction>
	
	<cffunction name="getAssetContent" access="public" returntype="string" output="false">
		<cfargument name="sAssetName" type="string" required="yes">
		<cfargument name="sAssetType" type="string" required="no" default="application">
        
        <cfset var sExtension = "">
        <cfset var sAssetScope = "">
        <cfset var pageletURL = "">
		<cfset var outputBuffer = "">
        
        <!--- first ensure that the asset name contains more than a single entity --->
        <cfif listLen(arguments.sAssetName, ".") LTE 0>
            <cfthrow message="The asset name does not contain enough information to determine its location. The asset requested was '#arguments.sAssetName#'.">
        </cfif>
        
		<!--- construct the pagelet url --->
		<cfset sExtension = listGetAt(arguments.sAssetName, 1, ".")>
        <cfset sAssetScope = instance.rhino.getEvent().getExtensionScope(sExtension)>
		
		<!--- Rhino assumes that a lack of assetscope places the request in the applications scope --->
		<cfif sAssetScope IS "">
			<cfset sAssetScope = "application">
			<cfset pageletURL = replace(arguments.sAssetName, ".", "/", "ALL") & ".cfm">
		<cfelse>
			<cfset pageletURL = listDeleteAt(replace(arguments.sAssetName, ".", "/", "ALL"), 1, "/") & ".cfm">
		</cfif>
		
        <cfif sAssetScope IS "application">
			<cfsavecontent variable="outputBuffer"><cfinclude template="#instance.rhino.roots().applicationRoot#/pagelets/#pageletURL#"></cfsavecontent>
        <cfelseif sAssetScope IS "appextension">
			<cfsavecontent variable="outputBuffer"><cfinclude template="#instance.rhino.roots().applicationRoot#/extensions/#sExtension#/pagelets/#pageletURL#"></cfsavecontent>
        <cfelseif sAssetScope IS "coreextension">
			<cfsavecontent variable="outputBuffer"><cfinclude template="#instance.rhino.roots().rhinoRoot#/extensions/#sExtension#/pagelets/#pageletURL#"></cfsavecontent>
		<cfelse>
            <cfif instance.rhino.config().devstate IS NOT "LIVE">
                <cfsavecontent variable="outputBuffer"><cfoutput><p>Development Error: Unable to load an asset with undefined type.<br/>
                    The type passed in was '#sAssetScope#'</p></cfoutput></cfsavecontent>
            </cfif>
		</cfif>
		
		<cfreturn outputBuffer>
	</cffunction>

    <cffunction name="dumpVars">
        <cfdump var="#variables#">
    </cffunction>

</cfcomponent>