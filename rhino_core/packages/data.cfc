<cfcomponent output="false">
    
<!---// The provisions of the data cfc are to assist in the construction and persistance of ColdFusion
CFC and other data objects for the duration of a request. The cfc has two accessors to data:

getData() - uses a predefined XML file (datasets.xml) to establish which data to instantiate and return
to the requestor.
getObject() - uses knowledge of the state of the application and the provided path information to return
any CFC that resides in one of a limited set of data locations.
--->

<cfset instance.datacache = structNew()>

<cffunction name="init" access="public" output="false">
    <cfargument name="refRhino" type="rhino" required="true">
    <cfargument name="attributes" type="struct" required="true">

    <cfset instance.rhino = arguments.refRhino>
	<cfset instance.applicationDotRoot = instance.rhino.roots().applicationDotRoot>
	<cfset instance.rhinoDotRoot = instance.rhino.roots().rhinoDotRoot>
	
	<cfset variables.attributes = arguments.attributes>
	
    <cfreturn this>
</cffunction>

<cffunction name="useMapping" access="public" output="false" description="gives the mapping names to the core and applications, otherwise the default (non-mapping) values will be used">
	<cfargument name="appMapping" type="string" required="true">
	<cfargument name="rhinoMapping" type="string" required="true">
	
	<cfif arguments.appMapping IS NOT "">
		<cfset instance.applicationDotRoot = arguments.appMapping>
	</cfif>
	
	<cfif arguments.rhinoMapping IS NOT "">
		<cfset instance.rhinoDotRoot = arguments.rhinoMapping>
	</cfif>
	
	<cfreturn this>
</cffunction>

<cffunction name="getData" access="public" output="false">
	<cfargument name="dataset" type="string">
	<cfargument name="type" type="string">
	<cfargument name="cache" type="boolean" default="true">
	
	<cfset var hashDataset = replace(arguments.dataset, ".", "__", "ALL")>
	
	<!--- basically this works by first attempting to invoke the component and then
	if successful, introspecting the arguments required and verifying they exist,
	caching this information. Then attempting to call the data if the parameters
	exist and then caching the result unless told otherwise.
	
	The dataset could be configured before hand in the datasets.xml file where
	url to argument mappings could have been made or it could be left to this component
	to attempt to gather together the relevant attributes and pass them in. --->
	<cfset var strParams = structNew()>
	
	
	<!--- if the data is already cached and caching is ok, then return it --->
	<cftry>
		<cfif cache AND structKeyExists(instance.datacache, hashDataset)>
			<cfreturn instance.datacache[hashDataset].data>
		</cfif>
		
		<cfcatch><cfrethrow></cfcatch>
	</cftry>
    
	<!--- is the data defined in the XML? --->
	<cfif (dataDefined(dataset))>
	
		<!--- get the data from the 'parameters' key of the structure into the instance array --->
		<cfset strDataset = instance.rhino.getParsed().datasets[dataset]>
		
		<cfset arrReqParams = strDataset.parameters>
		
	    <!--- if an argument type is specified then use that, if not check the XML, if not assume local --->
		<cfif structKeyExists(arguments, "type") AND arguments.type IS NOT "">
        <cfelseif structKeyExists(strDataset, "type")>
            <cfset arguments.type = strDataset.type>
        <cfelse>
            <cfset arguments.type = "local">
        </cfif>
		
		<!--- when no arguments are required, a strParams structure must still be defined --->
		<cfloop from="1" to="#arrayLen(arrReqParams)#" index="iParam">
			<cftry>
				<cfset sParamName = arrReqParams[iParam][1]>
				<cfset sParamAlias = arrReqParams[iParam][2]>
				<cfset sParamUniqueName = arrReqParams[iParam][3] & arrReqParams[iParam][1]>
				<cfset fRequired = arrReqParams[iParam][6]>
				
				<!--- if supposed to exist in the url/form scope, test for it --->
				<cfif arrReqParams[iParam][3] EQ "url">
					<cfif structKeyExists(attributes, sParamName)>
						<cfset strParams[sParamAlias] = attributes[sParamName]>
					<cfelseif structKeyExists(url, sParamName)>
						<cfset strParams[sParamAlias] = url[sParamName]>
					<cfelseif structKeyExists(form, sParamName)>
						<cfset strParams[sParamAlias] = form[sParamName]>
					<cfelse>
						<cfthrow type="mvcvalidation"
							message="MVC page #request.layout# requires the url or form variable: #arrReqParams[iParam][1]#">
					</cfif>
				
				<!--- if supposed to exist in the cookie scope, test for it --->
				<cfelseif arrReqParams[iParam][3] EQ "cookie">
					<cfif structKeyExists(cookie, sParamName)>
						<cfset strParams[sParamAlias] = cookie[sParamName]>
					<cfelse>
						<cfthrow type="mvcvalidation"
							message="MVC page #request.layout# requires the cookie: #arrReqParams[iParam][1]#">
					</cfif>
				
				<!--- if the variable is a fixed value, then read it in and set it --->
				<cfelseif arrReqParams[iParam][3] EQ "value">
					<cfset strParams[sParamAlias] = arrReqParams[iParam][1]>
					
				<!--- otherwise try and find it in another scope (request, application, session etc) and error if not found --->
				<cfelse>
					<cfif isDefined("#arrReqParams[iParam][3]#.#arrReqParams[iParam][1]#")>
						<cfset strParams[sParamAlias] = evaluate("#arrReqParams[iParam][3]#.#arrReqParams[iParam][1]#")>
					<cfelse>
						<cfthrow type="mvcvalidation"
							message="MVC page #request.layout# requires the parameter: #arrReqParams[iParam][1]#">
					</cfif>
				</cfif>
				
				<cfcatch type="mvcvalidation">
					<!--- check if there is any corrective action specified --->
					<cfif arrReqParams[iParam][4] NEQ 0>
						<cfset sCorrective = arrReqParams[iParam][5]>
						<cfif arrReqParams[iParam][4] EQ "location"><cflocation url="/?layout=#sCorrective#&returnurl=#urlencodedformat(cgi.SERVER_NAME&"/index.cfm?"&cgi.query_string)#" addtoken="no">
						<cfelseif arrReqParams[iParam][4] EQ "default"><cfset strParams[sParamAlias] = sCorrective>
						<cfelse><cfrethrow></cfif>
					<cfelse>
						<cfrethrow>
					</cfif>
				</cfcatch>
			</cftry>
		</cfloop>
		
		<!--- Now we call the data --->
		<cfif arguments.type EQ "global">
			<cftry>
				<cfinvoke component="components.#strDataset.location#" returnvariable="varTemp" method="#strDataset.method#" argumentcollection="#strParams#">
				<cfcatch>
					<cfrethrow>
				</cfcatch>
			</cftry>
		<cfelseif arguments.type EQ "local" AND listGetAt(dataset, 1, ".") EQ instance.rhino.getName()>
			<cftry>
				<cfinvoke component="applications.#instance.rhino.getName()#.components.#strDataset.location#" returnvariable="varTemp" method="#strDataset.method#" argumentcollection="#strParams#">
				<cfcatch>
					<cfthrow message="Framework generated error: #cfcatch.message#"
						detail="#cfcatch.detail#">
				</cfcatch>
			</cftry>
		<cfelseif arguments.type EQ "local">
			<cftry>
				<cfinvoke component="extensions.#listGetAt(strDataset.name, 1, ".")#.components.#strDataset.location#" returnvariable="varTemp" method="#strDataset.method#" argumentcollection="#strParams#">
				<cfcatch>
					<cfrethrow>
				</cfcatch>
			</cftry>
		</cfif>
		<cfset instance.datacache[hashDataset] = structNew()>
        <cfif structKeyExists(variables, "varTemp")>
			<cfif NOT isObject(varTemp)>
				<cfset instance.datacache[hashDataset].data = duplicate(varTemp)>
			<cfelse>
				<cfset instance.datacache[hashDataset].data = varTemp>
			</cfif>
    		<cfreturn instance.datacache[hashDataset].data>
    	<cfelse>
    	    <cfreturn>
    	</cfif>
		
	<cfelse>
	
		Couldn't find <cfoutput>#dataset#</cfoutput>
		<cfreturn>
			
		<!--- in this case, we instantiate the component --->
		<cfif listGetAt(dataset, 1, ".") EQ instance.getName()>
			<cfset datalocation = listDeleteAt(arguments.dataset, 1, ".")>
			<cftry>
				<cfscript>instance.datacache[#hashDataset#] = compcreateobject("component", "applications.#instance.appName#.components.#datalocation#");</cfscript>
				<cfcatch><cfrethrow></cfcatch>
			</cftry>
			<cfreturn instance.datacache[hashDataset].comp>
		<cfelseif arguments.type EQ "local">
			<cftry>
				<cfinvoke component="extensions.#listGetAt(strDataset.name, 1, ".")#.components.#strDataset.location#" returnvariable="varTemp" method="#strDataset.method#" argumentcollection="#strParams#">
				<cfcatch>
					<cfrethrow>
				</cfcatch>
			</cftry>
		<cfelse>
			No, that didn't work<cfabort>
		</cfif>
	
	</cfif>
	
	
	<!--- assemble the various head data from the calls in the XML --->
	<cfset arrHead = request.page.getHead()>
	<cfset sDynamicHeadTags = "">
	<cfloop from="1" to="#arrayLen(arrHead)#" index="i">
		<cfset arrHeadContent = arrHead[i]>
		<cfif arrayLen(arrHeadContent) EQ 2>
			<cfset sOpenTag = "<" & arrHeadContent[1] & ">">
			<cfset sCloseTag = "</" & arrHeadContent[1] & ">">
			<cfset sTagContent = "">
			<cfloop from="1" to="#arrayLen(arrHeadContent[2])#" index="j">
				<cfif left(arrHeadContent[2][j], 4) EQ "dyn:">
					<!--- evaluate the expression --->
					<cfset sTagContent = sTagContent & " " & evaluate(right(arrHeadContent[2][j], len(arrHeadContent[2][j])-4))>
				<cfelse>
					<cfset sTagContent = sTagContent & " " & arrHeadContent[2][j]>
				</cfif>
			</cfloop>
			<cfset sTag = sOpenTag & sTagContent & sCloseTag>
		<cfelseif arrayLen(arrHeadContent) EQ 3>
			<cfset sOpenTag = "<" & arrHeadContent[1]>
			<cfset sCloseTag = ">">
			<cfset sAttributes = "">
			<cfloop from="1" to="#arrayLen(ArrHeadContent[3])#" index="j">
				<cfif left(arrHeadContent[3][j][2], 4) EQ "dyn:">
					<!--- evaluate the expression --->
					<cfset sAttributes = sAttributes & " " & arrHeadContent[3][j][1] & "=""" & evaluate(right(arrHeadContent[3][j][2], len(arrHeadContent[3][j][2])-4)) & """">
				<cfelse>
					<cfset sAttributes = sAttributes & " " &  arrHeadContent[3][j][1] & "=""" & arrHeadContent[3][j][2] & """">
				</cfif>
			</cfloop>
			<cfset sTag = sOpenTag & sAttributes & sCloseTag>
		</cfif>
		<cfset sDynamicHeadTags = sDynamicHeadTags & sTag & Chr(13) & Chr(10)>
	</cfloop>
	
	<!--- check for the dynamic association of stylesheet and if not defined use default>
	<cfparam name="instance.rhino.getParsed().locals.stylesheet" default="/master.css" type="string" --->
	<!--- Start tag processing --->
</cffunction>

<cffunction name="dataDefined" access="private" returntype="boolean" output="false">
	<cfargument name="sDataset" type="string">
	<!---Looking for <cfoutput>#sDataset#</cfoutput>
	<cfdump var="#instance.rhino.getParsed().datasets#"><cfabort> --->
	<cfreturn structKeyExists(instance.rhino.getParsed().datasets, sDataset)>
</cffunction>

<!--- Used to obtain a reference to an instantiated instance of the specified object. If the object
has already been instantiated in this request and caching was not explicitly rejected, a cached 
instance of the object will be returned.

If the object is not within cache, the object will be instantiated and cached (or not) as requested.
the duration of the cache is for the duration of the request.
--->
<cffunction name="getObject" access="public" returntype="any" output="false">
	<cfargument name="objectName" type="string" required="true" hint="Name of the component to be returned, provide path relative to components directory using dots as separators">
	<cfargument name="type" type="string" required="false" default="local">
	<cfargument name="cache" type="boolean" required="false" default="true" hint="specify whether to use a previous cached instance and whether to store this instance in cache">
    
	<cfset var hashDataset = "rhinoobject" & replace(arguments.objectName, ".", "__", "ALL")>
    <cfset var componentScopeString = getToken(arguments.objectName, 1, ".")>
    <cfset var objectPath = "">
    <cfset var objTemp = {}>
    
	<!--- Analyse the requested object name.
		With only a single item in the name list, assume application scope
		With more than a single item, attempt to match the first item to an extension name, if it
			matches, then use it. If no match, then again, assume appliction scope.
		CAUTION: Care should be taken when you wish to use an application scoped data object
			without explicitly using the application's name prefix. If the first part of the
			data object's name matches a loaded extension, it will be assumed that the requested
			object is part of the matching extension, and will cause an error. When in doubt, use
			and explicity application/extension reference.
	--->
    <cfif listLen(arguments.objectName, ".") LT 2>
        <cfset objectScope = "application">
        <cfset objectPath = arguments.objectName>		
    <cfelseif instance.rhino.getEvent().isLoadedExtension(componentScopeString)>
        <cfset objectScope = instance.rhino.getEvent().getExtensionScope(componentScopeString)>
        <cfset objectPath = listDeleteAt(arguments.objectName, 1, ".")>
    <cfelse>
        <cfset objectScope = "application">
        <cfset objectPath = arguments.objectName>
		<!---
        <cfthrow message="Specified object does not exist in a known location and is not cached."
            detail="The object you are referencing '#arguments.objectName#' is in neither the 'application' nor the 'core' location and is not a cached object. Please check your spelling and try again.">
		--->
    </cfif>
    
	<cftry>
		<!--- if the data is already cached and caching is ok, then return it --->
		<cfif cache AND structKeyExists(instance.datacache, hashDataset)>
			<cfreturn instance.datacache[hashDataset].data>
		</cfif>
		<!--- create the hashkey to store the data once fetched, unless it exists --->
		<cfif NOT structKeyExists(instance.datacache, hashDataset) AND arguments.cache>
			<cfset instance.datacache[hashdataset] = structNew()>
		</cfif>
        
		<!--- fetch the data and store it --->
		<cftry>
            <!--- parse out some parts of the objectName --->
            <cfset extName = listGetAt(objectName, 1, ".")>
            <cfset pathToComp = listDeleteAt(objectName, 1, ".")>
            <!--- depending on the objectName, instantiate the appropriate component --->
	        <cfif lcase(arguments.type) IS "global">
				<cfscript>objTemp = createObject("component", "components.#replace(objectName, ".", "/", "ALL")#");</cfscript>
			<cfelseif objectScope IS "application">
				<cfscript>objTemp = createObject("component", "#instance.rhino.roots().applicationDotRoot#.components.#objectPath#");</cfscript>
			<cfelseif objectScope IS "appExtension">
				<cfscript>objTemp = createObject("component", "#instance.rhino.roots().applicationDotRoot#.extensions.#componentScopeString#.components.#objectPath#");</cfscript>
	        <cfelseif objectScope IS "coreExtension">
				<cfscript>objTemp = createObject("component", "#instance.rhino.roots().rhinoDotRoot#.extensions.#componentScopeString#.components.#objectPath#");</cfscript>
	        <cfelse>
                <cfthrow message="Unable to provide access to specified object">
			</cfif>
			
			<cfcatch>
				<cfthrow message="Unable to load the requested object."
						detail="Rhino Data object attempted to load ""#arguments.objectName#"" but encountered the following error: #cfcatch.message#. The component requested had the scope of: #componentScopeString#/#instance.rhino.getName()#" 
						extendedinfo="#cfcatch.message# #cfcatch.detail#">
			</cfcatch>
		</cftry>
        
		<!--- if we are to cache this object then do so --->
		<cfif arguments.cache>
			<cfset instance.datacache[hashDataset].data = objTemp>
		</cfif>
		
        <!--- if we encounter an error then throw it --->
		<cfcatch><cfrethrow></cfcatch>
	</cftry>
	<cfreturn objTemp>
</cffunction>

<!--- some more complete caching --->
<cffunction name="cacheObject" access="public" output="false" hint="allows for data objects to be pushed onto the cache stack for this request">
    <cfargument name="objIncoming" required="true">
    <cfargument name="sName" required="true" type="string">
    <cfset Var hashDataset = "rhinoobject" & replace(arguments.sName, ".", "__", "ALL")>
    <cfset instance.datacache[hashdataset] = structNew()>
    <cfset instance.datacache[hashDataset].data = objIncoming>
</cffunction>

    <!--- provide an alias to the wire objects wireObject() method --->
    <cffunction name="wireObject" access="public" returntype="any" output="false">
        <cfargument name="baseComponentName" required="true">
        <cfargument name="useCache" required="false" default="true">
        <cfargument name="callInit" required="false" type="boolean" default="true">
        
        <cfset var ret = request.wire.wireObject(baseComponentName, useCache, callInit)>
        
        <cfreturn ret>
    </cffunction>

</cfcomponent>
