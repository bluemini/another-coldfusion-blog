<cfcomponent output="false">

	<!--- the rhino cfc represents the application from the initial request --->
	<cfscript>
        instance.config = structNew();          // this will store framework configuration
        instance.config.devstate = false;       // this stores the development state, default to false
                                                // which means that config parsing will only occur on
                                                // framework initialisation. Set to true, to parse
                                                // on each request
        instance.config.state = "LIVE";         // this stores the development state, default to false
                                                // which means that config parsing will only occur on
                                                // framework initialisation. Set to true, to parse
                                                // on each request
        instance.isInitialised = false;
        
        instance.roots = structNew();
        instance.parsed = structNew();
        instance.appName = "Non";
        
	</cfscript>
	
	<cffunction name="init" access="public" output="false">
		<cfargument name="appName" type="string" required="false" default="#application.applicationName#">
		<cfscript>
        instance.appName = arguments.appName;

        // The default file locations for application root
        instance.roots.applicationDotRoot = "applications.#instance.appName#";
        instance.roots.applicationRoot = "/#replace(instance.roots.applicationDotRoot, ".", "/", "ALL")#";
        instance.roots.applicationRootPath = getDirectoryFromPath(expandPath("#instance.roots.applicationRoot#/"));
        
        // default location for application components
        instance.roots.rhinoDotRoot = "rhino_core";
        instance.roots.rhinoRoot = "/#replace(instance.roots.rhinoDotRoot, ".", "/", "ALL")#";
        instance.roots.rhinoRootPath = getDirectoryFromPath(expandPath("#instance.roots.rhinoRoot#/"));

        // instantiate the event and log objects into the rhino.cfc object
        instance.eventObject = createObject("component", "rhino_core.packages.event").init(this);
        instance.logObject = createObject("component", "rhino_core.packages.log").init(this);
        
        // Open up the configuration parser and load the base rhino configuration
        instance.parserObject = createObject("component", "rhino_core.packages.parser").init(this);
	 	// request.data = createObject("component", "rhino_core.packages.data").init(this);
	 	
		// instance.setupRhino();
        </cfscript>
    </cffunction>

	<!--- setupRhino()
	Sets up the rhino struct in the application scope, checks to see if this is a new
	session start (by the non existence of a session variable) and a new application
	start (by the non existence of an application variable). It calls startApplication() 
    and startSession() based on these findings.
	--->
    <cffunction name="setupRhino" access="public" output="false">
        <!--- run the initialisation of the application scope and config parsing if application 
        is starting or a reindex is requested. --->
        <cfif (NOT instance.isInitialised OR structKeyExists(url, "rhinoappstart") OR (instance.config.devstate AND instance.config.rhinoconfig.reparse_dev))>
            <cflock name="rhinoAppStart" type="exclusive" timeout="5">
                <cfif (NOT instance.isInitialised OR structKeyExists(url, "rhinoappstart") OR (instance.config.devstate AND instance.config.rhinoconfig.reparse_dev))>
                    <cfset init()>
                    <cfset startApplication()>
                </cfif>
            </cflock>
        </cfif>

        <!--- Run the initialisation of the session scope and config parsing if application is 
        starting or a reindex is requested. To optimise the code, we will lock only after we 
        are sure we have to, that way, once we are in production, we shouldn't need to lock 
        at all. This should also eliminate the possibility that under heavy load, multiple 
        requests cause the application to initialise multiple times --->
        <cfif NOT structKeyExists(session, "isInitialised") 
				OR (structKeyExists(session, "isInitialised") AND not session.isInitialised) 
					OR structKeyExists(url, "rhinosessionstart")>
            <cflock name="rhinoSessionStart" type="exclusive" timeout="5">
                <cfif NOT structKeyExists(session, "isInitialised") 
						OR (structKeyExists(session, "isInitialised") AND not session.isInitialised) 
							OR structKeyExists(url, "rhinosessionstart")>
                    <cfset startSession()>
                </cfif>
            </cflock>
        </cfif>
    
        <cfreturn this>
    </cffunction>
	
	<cffunction name="startApplication" access="private" output="false">
        
        <cfset var configParser = "">
		<cfset var appConfig = {}>
        
		<!--- the only reason not to run loadConfiguration first is that url.reindex might be supplied in SES format and therefore won't be found
		so if you need to reindex, you MUST use proper url instance --->
		<!--- set the root location of the rhino_core for use by all other templates where required --->
		
		<!--- initialise the application structures --->
		<cflock scope="application" type="exclusive" timeout="5">
			<cfscript>
			structClear(instance.parsed);
			instance.parsed.locals = structNew();
			instance.parsed.pagelets = structNew();
			instance.parsed.datasets = structNew();
			instance.parsed.pages = structNew();
			instance.parsed.controllers = structNew();
			instance.parsed.wires = structNew();
			
			// load the rhino config and explicitly set the log and state options
			instance.parserObject.loadRhinoConfig();
            instance.logObject.enableLog(instance.config.rhinoConfig.rhino_log);
			if (structKeyExists(instance.config.rhinoConfig, "rhino_state")) {
				instance.config.state = ucase(instance.config.rhinoConfig.rhino_state);
				if (instance.config.rhinoConfig.rhino_state IS "DEV") {
			    	instance.config.devstate = true;
			    }
			}

			// Load application's own cfc if it exists. This uses event.cfc to register the various capabilities of the applications primary component
			loadApp();

            // read in any application specific extensions, this needs the event cfc to be loaded.
            instance.eventObject.readExtensions("app");
            
			if (structKeyExists(instance.config.rhinoConfig, "params") AND structKeyExists(instance.config.rhinoConfig.params, "rhino_state") AND instance.config.rhinoConfig.params.rhino_state IS "DEV") {
		    	instance.config.devstate = true;
			}
			
			// Load the current extensions
			instance.eventObject.callHandler("preXMLRead");

			// Load applications configuration (pages, pagelets, datasets) files
			appConfig = instance.parserObject.loadConfig(instance.appName);
			saveParsed(appConfig);
			
			// call the onAppStartPostParse event
			instance.eventObject.callHandler("onAppStartPostParse");
			
			// finally set the isInitialised key
			instance.isInitialised = true;
			
            </cfscript>
			
		</cflock>
	</cffunction>
	
	<cffunction name="startSession" access="private" output="false">
		<!--- limited functionality at the moment, simply calls an extension with the onSessionStartPostParse() event --->
		<cfset session.isInitialised = true>
        <cftry>
    		<cfset instance.eventObject.callHandler("onSessionStartPostParse")>
    		<cfcatch>
                <cfset session.isInitialised = false>
            </cfcatch>
        </cftry>
	</cffunction>
	
	<cffunction name="loadApp" access="private" output="false">
        
        <cfset var compTemp = {}>
        
		<!--- this attempts to locate and load a specific application file --->
		<cfif fileExists(instance.roots.applicationRootPath&"/"&instance.appName&".cfc")>
			<!--- instantiate the application's cfc --->
			<cfset compTemp = createObject("component", "#instance.roots.applicationDotRoot#.#instance.appName#")>
			
			<!--- if the cfc has an init method, then call it --->
			<cfif StructKeyExists(compTemp, "init")>
				<cfset compTemp.init()>
			</cfif>
			
            <!--- add the public methods of the applications cfc to the event framework --->
            <cfset instance.eventObject.cacheExtension(compTemp, "", "application")>
        
        <cfelse>
            <cfthrow message="No component found for #instance.appName# in the root folder"
                detail="Rhino was looking for ""#instance.roots.applicationDotRoot#/#instance.appName#.cfc">
		</cfif>
        
	</cffunction>
	
	<!--- save the parsed values from the application and extensions into the internal 'parsed' cache --->
	<cffunction name="saveParsed" access="package" output="false">
		<cfargument name="parsedData" type="struct">
		<cfscript>
			// store the locals
			if (structKeyExists(parsedData, "locals")) {
				instance.parsed.locals = mergeStruct(instance.parsed.locals, parsedData.locals);
			}
			// store the datasets
			if (structKeyExists(parsedData, "datasets")) {
				instance.parsed.datasets = mergeStruct(instance.parsed.datasets, parsedData.datasets);
			}
			// store the pages
			if (structKeyExists(parsedData, "pages")) {
				instance.parsed.pages = mergeStruct(instance.parsed.pages, parsedData.pages);
			}
			// store the controllers
			if (structKeyExists(parsedData, "controllers")) {
				instance.parsed.controllers = mergeStruct(instance.parsed.controllers, parsedData.controllers);
			}
			
			// store and wiring information
			if (structKeyExists(parsedData, "wires")) {
				instance.parsed.wires = mergeStruct(instance.parsed.wires, parsedData.wires);
			}
		</cfscript>
	</cffunction>
	<cffunction name="mergeStruct" access="private" output="false">
		<cfargument name="structOne" type="struct">
		<cfargument name="structTwo" type="struct">
		<!--- we are going to merge structTwo into structOne --->
		<cfset var hostKeys = structKeyArray(structOne)>
		<cfset var newKeys = structKeyArray(structTwo)>
		
		<cfloop from="1" to="#arrayLen(newKeys)#" index="iNewKey">
			<cfset newKey = newKeys[iNewKey]>
			<cfif structKeyExists(structOne, newKey)>
				<cfif isStruct(structOne[newKey]) AND isStruct(structTwo[newKey])>
					<!--- if both the sub keys are also structs, then we merge them --->
					<cfset mergeStruct(structOne[newKey], structTwo[newKey])>
				</cfif>
			<cfelse>
				<!--- if the key doesn't exist, then we create it on the host and add the values --->
				<cfset structOne[newKey] = structTwo[newKey]>
			</cfif>
		</cfloop>

		<cfreturn structOne>
	</cffunction>
	
    <!--- The framework only loads extensions when the application either
    requests that all extensions are loaded or when it explicitly requests that
    it is loaded --->
	<cffunction name="moduleRequired" access="package" output="false" returntype="numeric">
		<cfargument name="sModuleName" type="string">
        
		<!--- 0 = not required, 1 = load if there, 2 = Required, error if not present --->
        <cfif NOT structKeyExists(instance, "modules")>
            <cfset io = instance.eventObject.callHandler("getModules", instance.appName, true)><!--- this.instance.base.getModules() --->
            <cfif io.set>
                <cfif isStruct(io.data)>
                    <cfset instance.modules = io.data>
                <cfelse>
                    <cfset instance.modules = structNew()>
                </cfif>
            <cfelse>
                <cfthrow message="[Core] #instance.appName#.cfc does not contain the getModules() method to check extensions.">
            </cfif>
        </cfif>
		<cfif structKeyExists(instance.modules, "denyall")>
			<cfreturn 0>
		<cfelseif structKeyExists(instance.modules, arguments.sModuleName) AND instance.modules[sModuleName] GT 0 AND instance.modules[sModuleName] LTE 2>
			<cfreturn instance.modules[sModuleName]>
		<cfelseif structKeyExists(instance.modules, "allowAll")>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
    
    <cffunction name="getRhinoIndexFile" access="public" returntype="string" output="false">
        <cfreturn instance.config.rhinoConfig.rhinoindex & ".cfm">
    </cffunction>
	
    <!--- provides access to elements of the locals.xml file in the application config.--->
    <cffunction name="getLocal" access="public" returntype="string" output="false">
        <cfargument name="sLocal" type="string" required="true">
		<cfargument name="throwOnNotFound" type="boolean" required="false" default="false">
		
        <cfif structKeyExists(instance.parsed.locals, sLocal)>
            <cfreturn instance.parsed.locals[sLocal]>
        <cfelseif arguments.throwOnNotFound>
			<cfthrow message="local configuration parameter '#arguments.slocal#' cannot be found.">
		<cfelse>
            <cfreturn "">
        </cfif>
    </cffunction>
	
    <!--- provides access to elements of the locals.xml file in the application config.--->
    <cffunction name="getWire" access="public" returntype="string" output="false">
        <cfargument name="wireAlias" type="string" required="true">
		
        <cfif structKeyExists(instance.parsed.wires, arguments.wireAlias)>
            <cfreturn instance.parsed.wires[arguments.wireAlias].class>
        <cfelse>
            <cfreturn "">
        </cfif>
    </cffunction>
	
	<!--- provides a context that can be passed to objects to reduce the tight coupling between
	CFRhino and the external object. Generally, the context includes entries from the locals
	file. It also contains a reference back to this object, although this can tightly couple
	an external object, since this may not exist otherwise. --->
	<cffunction name="getContext" access="public" output="false">
		<cfset var context = structNew()>
		<cfset context = mergeStruct(context, instance.parsed.locals)>
		<cfreturn context>
	</cffunction>
    
    <cffunction name="getLog" access="public" output="false">
        <cfreturn instance.logObject>
    </cffunction>
    <cffunction name="getEvent" access="public" output="false">
        <cfreturn instance.eventObject>
    </cffunction>
    <cffunction name="getParser" access="package" output="false">
        <cfreturn instance.parserObject>
    </cffunction>
    <cffunction name="roots" access="public" output="false" returntype="struct">
        <cfreturn instance.roots>
    </cffunction>
    <cffunction name="config" access="public" output="false" returntype="struct">
        <cfreturn instance.config>
    </cffunction>
	
	<!--- provide framework access to the application's name --->
	<cffunction name="getName" access="public" output="false" returntype="string">
		<cfreturn instance.appName>
	</cffunction>
	
	<!--- lookup the type of versions that a particular page is available for --->
	<cffunction name="getVersions" access="package" returntype="array" output="false">
		<cfargument name="pageName" type="string" required="true">
		<cfset var versions = ArrayNew(1)>
		<cfif pageExists(pageName)>
			<cfset versions = StructKeyArray(instance.parsed.pages[pageName])>
		</cfif>
		<cfreturn versions>
	</cffunction>
	
	<!--- provide a means to ask about the components state of initialisation --->
	<cffunction name="isInitialised" access="public" output="false" returntype="boolean">
		<cfreturn instance.isInitialised>
	</cffunction>
	
	<!--- return the 'state' string (defaults to 'DEV') for the current application. --->
	<cffunction name="getState" access="public" output="false" returntype="string">
		<cfreturn instance.config.state>
	</cffunction>
    
    <cffunction name="setConfig" access="package" output="false">
        <cfargument name="config" type="struct">
        <cfset instance.config.rhinoConfig = arguments.config>
    </cffunction>
	
	<cffunction name="pageExists" access="public" returntype="boolean" output="false">
		<cfargument name="pageName" type="string" required="true">
		<cfif structKeyExists(instance.parsed.pages, arguments.pageName)>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>
	
	<cffunction name="controllerExists" access="public" returntype="boolean" output="false">
		<cfargument name="pageName" type="string" required="true">
		<cfif structKeyExists(instance.parsed.controllers, arguments.pageName)>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>
	
	<cffunction name="getParsed" access="public" returntype="any" output="false">
		<cfargument name="pageName" type="string" required="false" default="">
		<cfargument name="sectionName" type="string" required="false" default="">
		<cfargument name="location" type="string" required="false" default="pages">
		<cfargument name="version" type="string" required="false" default="default" hint="If requesting a page configuration for a particular version (eg mobile) it is passed here and checked for existence">
		
		<!--- if pageName and/or sectionName are empty, we return chunks of the struct --->
		<cfif arguments.sectionName IS "">
			<cfif arguments.pageName IS "">
				<cfreturn instance.parsed>
			<cfelseif arguments.pageName IS "ALLPAGES">
				<cfreturn instance.parsed.pages>
			<cfelseif arguments.pageName IS "ALLCONTROLLERS">
				<cfreturn instance.parsed.controllers>
			<cfelseif pageExists(arguments.pageName)>
				<!--- check if the rqeuest version exists and if not, return the default and if that doesn't exist
				return the first version that we have.  --->
				<cfif StructKeyExists(instance.parsed.pages[arguments.pageName], version)>
					<cfreturn instance.parsed.pages[arguments.pageName][version]>
				<cfelseif StructKeyExists(instance.parsed.pages[arguments.pageName], "default")>
					<cfreturn instance.parsed.pages[arguments.pageName].default>
				<cfelse>
					<cfreturn instance.parsed.pages[arguments.pageName][StructKeyArray(instance.parsed.pages[pageName])[1]]>
				</cfif>
			<cfelseif controllerExists(arguments.pageName)>
				<cfreturn instance.parsed.controllers[arguments.pageName]>
			</cfif>
			<cfthrow message="CFRhino Page Error: The requestsed page doesn't exist">		
		</cfif>
		
		<cftry>
			<cfswitch expression="#lcase(arguments.location)#">
				<cfcase value="pages">
					<cfif StructKeyExists(instance.parsed[arguments.location][arguments.pageName], version)>
						<cfreturn instance.parsed[arguments.location][arguments.pageName][version][arguments.sectionName]>
					<cfelse>
						<cfreturn instance.parsed[arguments.location][arguments.pageName].default[arguments.sectionName]>
					</cfif>
				</cfcase>
				<cfcase value="controllers">
					<cfreturn instance.parsed[arguments.location][arguments.pageName]>
				</cfcase>
				<cfdefaultcase>
					<cfreturn instance.parsed.pages[arguments.pageName][arguments.sectionName]>
				</cfdefaultcase>
			</cfswitch>
			<cfcatch>
				<cfreturn structNew() />
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>