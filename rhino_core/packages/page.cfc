<cfcomponent displayname="Page">
    
    <cfset variables.attributes = StructNew()>
	<cfset variables.version = "default">

	<cffunction name="init" access="public" output="false">
	    <cfargument name="refRhino" type="rhino" required="true">
	    <cfset variables.rhino = arguments.refRhino>
	    <cfreturn this>
	</cffunction>

	<cffunction name="mergeScopes" access="public" output="false">
		<cfargument name="urlScope" type="struct">
		<cfargument name="formScope" type="struct">
		<cfargument name="core" type="struct">
	
		<cfset structAppend(core, urlScope, false)>
		<cfset structAppend(core, formScope, false)>
		
		<cfset variables.attributes = arguments.core>
	</cffunction>

	<cffunction name="getAttributes" access="public" output="false">
	    <cfreturn variables.attributes>
	</cffunction>

	<!--- if the application renders different versions of pages based on some criteria, then
	set the version for this page rendering here --->
	<cffunction name="setVersion" access="public" output="false">
		<cfargument name="versionName" type="string" required="true">
		<cfset variables.version = versionName>
	</cffunction>

	<cffunction name="processRequest" access="public" output="false" returntype="struct">
		<cfset var io = {}>
		<cfscript>
		variables.fSuccess = false;
		variables.fController = false;
		variables.lstCurrentPos = "";					// variable to allow the storage of the currentPos array as a text list
		variables.currentPos = arrayNew(1);				// currentPos holds stack values to track the build progress
		variables.currentPos[1] = 0;					// initialise the stack (we start at zero cos the first op the getNextPagelet is to increment it
		variables.flayoutrendered = false;
		variables.fPageletOk = true;
		variables.intercept = false;			
		variables.interceptby = "";						// intercept is whether we ask Rhino to intercept the page call and divert it to an extension call
		variables.pageScope = "application";
		variables.currentSpacePagelet = 1;				// tracks the pointer to the current pagelet being used by the application
		variables.layout = "";
		variables.aborted = false;                      // tracks whether the entire page has been aborted, but we want to keep rendering a layout
		
		/*  set the correct layout to be rendered based on the existence of the url.layout param or the defaultLayout
			if an event has interrupted flow prior to here, it should have created/modified request.layout, in which
			case, if it exists, then use it. Otherwise, check the other sources in order (attributes (url), form, default)
		*/
	    if (StructKeyExists(request, "layout")) {
	        variables.layout = request.layout;
		} else if (isDefined("variables.attributes.layout")) {
			variables.rhino.getLog().addEntry("[Page] layout defined in 'attributes' set to: '#attributes.layout#'.");
			variables.layout = attributes.layout;
		} else if (isDefined("form.layout")) {
			variables.rhino.getLog().addEntry("[Page] layout defined in 'form' set to: '#form.layout#'.");
			variables.layout = form.layout;
		} else {
			variables.layout = variables.rhino.getName() & "." & variables.rhino.config().rhinoConfig.defaultLayout;
			variables.rhino.getLog().addEntry("[Page] layout undefined, set to rhino default: '#variables.layout#'.");
		}
		
		// added this in case the url contains two references to layout (or whatever) in which case
		// they are a comma separated list
		if (listLen(variables.layout) GT 1) {
			variables.layout = listGetAt(variables.layout, 1);
		}
		
		// establish if the application name needs to be prepended to the passed in layout
		if (variables.layout IS NOT "") {
			sPrimaryLayout = listGetAt(variables.layout, 1, ".");
	        variables.rhino.getLog().addEntry("[Page] Attempting to expand '#variables.layout#'.");
	        // if handled by the main application, just set the request scoped layout
			if (sPrimaryLayout IS variables.rhino.getName() ) {
				request.layout = variables.layout;
			
			// if handled by an extension from its own pages.xml or by interecption, set the flag
			} else if ( variables.rhino.getEvent().getInterceptAllowed(sPrimaryLayout)
						OR variables.rhino.getEvent().isLoadedExtension(sPrimaryLayout) ) {
				request.layout = variables.layout;
				variables.intercept = true;
				variables.interceptBy = sPrimaryLayout;
				variables.pageScope = variables.rhino.getEvent().getExtensionScope(sPrimaryLayout);
			
			// otherwise, prefix the layout with the application as a fallback
			} else {
				request.layout = variables.rhino.getName() & "." & variables.layout;
				sPrimaryLayout = variables.rhino.getName();
			}
		}
		variables.rhino.getLog().addEntry("[Page] The URL layout #variables.layout# has been parsed to #request.layout#.", 4);
	
		// if the request.layout exists then we use it directly, if not, call onRequestPagletUndefined 
		// and see if a default pagelet is returned
		if (variables.rhino.pageExists(request.layout)) {
			// the page was found in the application's pages.xml
			variable.location = "pages";
			/*
			variables.layouts = variables.rhino.getParsed(request.layout, "layouts", variable.location);
			variables.pagelets = variables.rhino.getParsed(request.layout, "pagelets", variable.location);
			variables.datasets = variables.rhino.getParsed(request.layout, "datasets", variable.location);
			// variables.modules = variables.rhino.getParsed().pages[request.layout].modules;
			variables.params = variables.rhino.getParsed(request.layout, "params", variable.location);
			variables.space = variables.rhino.getParsed(request.layout, "space", variable.location);
			variables.spacePagelets = variables.rhino.getParsed(request.layout, "spacePagelets", variable.location);
			variables.head = variables.rhino.getParsed(request.layout, "head", variable.location);
			*/
			StructAppend(variables, variables.rhino.getParsed(pageName=request.layout, version=variables.version));
			variables.fSuccess = true;
		} else if (variables.rhino.controllerExists(request.layout)) {
			// the page was found in the application's controllers.xml
			variable.location = "controllers";
			variables.controller = variables.rhino.getParsed(request.layout, "controller", variable.location);
			variables.layouts = arrayNew(1);
			variables.datasets = arrayNew(1);
			variables.params = structNew();
			variables.head = arrayNew(1);
			variables.fController = true;
			variables.fSuccess = true;
		} else {
			// finally we see if an extension is able to handle the request
			io = variables.rhino.getEvent().callHandler("onRequestPageUndefined", sPrimaryLayout, true);
			if (io.set) {
				tempData = io.data;
				// presuming that the extension returned a default handler, we should check that this exists
				if (isStruct(tempData)) {
		       	    variables.fSuccess = true;
		            variables.intercept = true;
		            variables.interceptby = io.handler;
	                if (tempData.handler IS NOT "controller") {
	                    variables.layouts = tempData.layouts;
	                    variables.pagelets = tempData.pagelets;
	                    variables.datasets = tempData.datasets;
	                    variables.modules = tempData.modules;
	                    variables.params = tempData.params;
	                    variables.space = tempData.space;
	                    variables.spacePagelets = tempData.spacePagelets;
	                    variables.head = tempData.head;
	                } else {
	                    variables.controller = tempData.controller;
	                    variables.layouts = arrayNew(1);
	                    variables.fController = true;
	                }
					/* if the handler sets a specific intercept scope, then we honour it. To remain
					compatible with previous code that might use interceptScope, we also check for that too
					*/
					if (structKeyExists(tempData, "pageScope")) {
						variables.pageScope = tempData.pageScope;
					} else if (structKeyExists(tempData, "interceptScope")) {
						variables.pageScope = tempData.interceptScope;
					}
				} else {
					// the data returned by the callhandler was not a struct and cannot be used
	                variables.rhino.getLog().addEntry("[Page] Call to onRequestPageUndefined() did not return a valid IO struct.", 2);
				}
			} else {
				// no data was returned from the callhandler
	            variables.rhino.getLog().addEntry("[Page] The requested layout cannot be found in the application, any extension or as a controller.", 1);
	            variables.rhino.getLog().addEntry("[Page] 404 Success=#variables.fSuccess#; Controller=#variables.fController#; Intercept=#variables.intercept#; Layout=#request.layout#", 1);
	        }
	    }
	    
		// Encoding
		variables.strCurrentNode = structGet("variables.construct");	// strCurrentNode is the node to be rendered
		// Each page will map a pagelet to a template space.
		</cfscript>
		
	    <cfif StructKeyExists(variables, "deleted") AND variables.deleted>
	        <cfheader statuscode="410" statustext="Gone">
			<cfthrow message="The page '#request.layout#' no longer exists." detail="#request.layout#" errorcode="410">
	    <cfelseif Not variables.fSuccess>
	        <cfheader statuscode="404" statustext="File Not Found">
	        <cfthrow message="#request.layout# cannot be found" detail="intercept set to #variables.intercept#, controller set to #variables.fController#" errorcode="404">
	    </cfif>
		
	    <!---  Set the encoding scheme for the application, if none is provide use UTF-8 --->
	    <cfif (variables.rhino.getlocal("urlEncoding") IS NOT "")><cfset setEncoding("url", variables.rhino.getlocal("urlEncoding"))><cfelse><cfset setEncoding("url", "UTF-8")></cfif>
	    <cfif (variables.rhino.getlocal("formEncoding") IS NOT "")><cfset setEncoding("form", variables.rhino.getlocal("formEncoding"))><cfelse><cfset setEncoding("form", "UTF-8")></cfif>
	    <cfif (variables.rhino.getlocal("pageEncoding") IS NOT "")><cfcontent type="text/html; charset=#variables.rhino.getlocal("pageEncoding")#"><cfelse><cfcontent type="text/html; charset=UTF-8"></cfif>
	
		<cfreturn this>
	</cffunction>


	<!--- methods which are called by the root page either to generate a layout or pass to a controller --->
	<cffunction name="getLayout" access="public" output="false" returntype="string">
	
		<cfif fController>
			<cfdump var="#variables#">
			<cfoutput>#variables.rhino.roots().applicationRoot#</cfoutput><br>
			<cfif (pageScope IS "application" OR pageScope IS "")>
				YEP<br/>
			</cfif>
			<cfoutput>#variables.rhino.getEvent().getExtensionScope(variables.interceptby)#</cfoutput>
		</cfif>
	
	    <cfscript>
		// calll teh onRequestStart event
		variables.rhino.getEvent().callHandler("onRequestStart");
		
	    // to simplify the index.cfm, we only call this one method and so we must first check for controller
	    if (variables.fController) {
	        sLayoutPath = replace(variables.controller, ".", "/", "ALL");
	        // pageScope = variables.rhino.getEvent().getExtensionScope(variables.interceptby);
	        if (pageScope IS "application" OR pageScope IS "") {
	            return "#variables.rhino.roots().applicationRoot#/controllers/#sLayoutPath#.cfm";
	        } else if (pageScope IS "appextension") {
	            return "#variables.rhino.roots().applicationRoot#/extensions/#variables.interceptby#/controllers/#sLayoutPath#.cfm";
	        } else if (pageScope IS "coreextension") {
	            return "#variables.rhino.roots().rhinoRoot#/extensions/#variables.interceptby#/controllers/#sLayoutPath#.cfm";
	        }
	    } else {
			sLayoutPath = replace(variables.layouts[1], ".", "/", "ALL");
			if (pageScope IS "") {
				pageScope = variables.rhino.getEvent().getExtensionScope(variables.interceptby);
			}
	        if (pageScope IS "application" OR pageScope IS "") {
	            return "#variables.rhino.roots().applicationRoot#/layouts/#sLayoutPath#.cfm";
	        } else if (pageScope IS "appextension") {
	            return "#variables.rhino.roots().applicationRoot#/extensions/#variables.interceptby#/layouts/#sLayoutPath#.cfm";
	        } else if (pageScope IS "coreextension") {
	            return "#variables.rhino.roots().rhinoRoot#/extensions/#variables.interceptby#/layouts/#sLayoutPath#.cfm";
	        }
	    }
	    </cfscript>
	</cffunction>

	<!--- cffunction name="getController" access="public" returntype="string">
		<cfset sLayoutPath = replace(variables.controller, ".", "/", "ALL")>
		<cfif variables.intercept>
			<cfreturn "extensions/#sPrimaryLayout#/controllers/#sLayoutPath#.cfm">
		<cfelse>
			<cfreturn "applications/#variables.rhino.getName()#/controllers/#sLayoutPath#.cfm">
		</cfif>
	</cffunction --->

	<cffunction name="getHead" access="public" output="false" returntype="array">
		<cfreturn variables.head>
	</cffunction>

	<cffunction name="getHandledBy" access="public" output="false" returntype="string">
	    <cfreturn variables.interceptby>
	</cffunction>

	<cffunction name="getScope" access="public" returntype="string" output="false">
		<cfreturn variables.pageScope>
	</cffunction>

	<!--- query method to see if we are dealing with a controller --->
	<cffunction name="isController" access="public" returntype="boolean">
		<cfreturn variables.fController>
	</cffunction>

	<!--- method to inform other components if we are dealing with an interupt condition --->
	<cffunction name="isInterupt" access="public" returntype="boolean">
	    <cfreturn variables.intercept>
	</cffunction>
	
	<!--- returns the versions available for the current page --->
	<cffunction name="getVersions" access="public" returntype="array" output="false">
		<cfif NOT StructKeyExists(variables, "versionsAvailable")>
			<cfset variables.versionAvailable = request.rhino.getVersions(request.layout)>
		</cfif>
		<cfreturn variables.versionAvailable>
	</cffunction>

	<cffunction name="getReqParams" access="public" output="false" returntype="struct">
		<cfreturn variables.params>
	</cffunction>

	<cffunction name="getReqParamVal" access="public" output="false" returntype="any">
		<cfargument name="param" required="true" type="any">
		<cfreturn variables.reqUrlParams[param]>
	</cffunction>

	<cffunction name="getReqDatasets" access="public" output="false" returntype="array">
		<cftry>
			<cfset returnRequired = variables.datasets>
			<cfcatch><cfthrow message="getReqDatasets() Error"></cfcatch>
		</cftry>
		<cfreturn variables.datasets>
	</cffunction>


	<!--- With the restructuring of the template data, this section deals with delivering the 
	pagelets for a given template space.
	
	Call 
	
	1.	setSpaceName(string space_name) setup data to begin rendering a space
	2.	getNextSpacePagelet() setup the next pagelet to render in the space (returns false when there are no further pagelets)
	3.	getNextSpacePageletName() returns the name of the pagelet to render. This function is idempotent and will only return a new value once getNextSpacePagelet() is called.
	4.	getNextSpacePageletType() returns the type of the pagelet to render. Idempotent as above
	5.	getNextSpacePageletScope() returns the scope of the pagelet (where to find it). Idempotent as above
	--->
	
	<!--- sets local data for a given space name --->
	<cffunction name="setSpaceName" access="public" returntype="boolean">
		<cfargument name="sSpaceName" type="string">
		<cfscript>
		variables.iCurrentPos = 0;
		if (StructKeyExists(variables.spacePagelets, sSpaceName)) {
			variables.arrSpacePagelets = variables.spacePagelets[sSpaceName];
		} else {
			variables.arrSpacePagelets = ArrayNew(1);
		}
		return true;
		</cfscript>
	</cffunction>
	
	<!--- If there is another pagelet to render, setup the pointer and return true, otherwise returns false --->
	<cffunction name="getNextSpacePagelet" access="public" returntype="boolean">
		<cfif variables.iCurrentPos LT arrayLen(variables.arrSpacePagelets)
				AND NOT variables.aborted>
			<cfset variables.iCurrentPos = variables.iCurrentPos + 1>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>
	
	<!--- return the name of the pagelet currently being pointed to --->
	<cffunction name="getNextSpacePageletName" access="public" returntype="string">
		<cfif variables.iCurrentPos LTE arrayLen(variables.arrSpacePagelets)>
			<cfreturn variables.arrSpacePagelets[variables.iCurrentPos].name>
		<cfelse>
			<cfthrow message="The requested pagelet doesn't exist in the current configuration."
					detail="You error should never occur since this function should only ever be called if page.getNextPagelet returns true, which is shouldn't have.">
		</cfif>
	</cffunction>
	
	<!--- return the scope of the pagelet currently being pointed to --->
	<cffunction name="getNextSpacePageletScope" access="public" returntype="string">
		<cfset scope = "">
		<cfif variables.iCurrentPos LTE arrayLen(variables.arrSpacePagelets)>
			<!--- if the scope is not yet defined, we try and resolve using the pagelets name. If
			that doesn't yield a valid name, then we assume it should be 'application'. We then save 
			the value into the cached struct. --->
			<cfif variables.arrSpacePagelets[variables.iCurrentPos].scope IS "">
				<cfset scope = request.rhino.getEvent().getExtensionScope(listGetAt(getNextSpacePageletName(), 1, "."))>
				<!--- if the response is not empty, then set into the cache --->
				<cfif scope IS "">
				    <cfset variables.arrSpacePagelets[variables.iCurrentPos].scope = "application">
				<cfelse>
				    <cfset variables.arrSpacePagelets[variables.iCurrentPos].scope = scope>
				</cfif>
			</cfif>
			<!--- then return it --->
			<cfreturn variables.arrSpacePagelets[variables.iCurrentPos].scope>
		<cfelse>
			<cfthrow message="The requested pagelet doesn't exist in the current configuration."
					detail="You error should never occur since this function should only ever be called if page.getNextPagelet returns true, which is shouldn't have.">
		</cfif>
	</cffunction>
	
	<!--- return the parent of the pagelet currently being pointed to. The parent is the hosting
	application or extension. Returns empty string if unable to determine. --->
	<cffunction name="getNextSpacePageletParent" access="public" returntype="string">
		<cfif variables.iCurrentPos LTE arrayLen(variables.arrSpacePagelets)>
			<!--- if the scope is not yet defined, we try and resolve using the pagelets name --->
			<cfif NOT StructKeyExists(variables.arrSpacePagelets[variables.iCurrentPos], "parent")>
				<cfset parentGuess = listGetAt(getNextSpacePageletName(), 1, ".")>
				<cfif request.rhino.getEvent().isLoadedExtension(parentGuess)>
				    <cfset variables.arrSpacePagelets[variables.iCurrentPos].parent = parentGuess>
				<cfelse>
					<cfset variables.arrSpacePagelets[variables.iCurrentPos].parent = "">
				</cfif>
			</cfif>
			<cfreturn variables.arrSpacePagelets[variables.iCurrentPos].parent>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction name="abortSpace" access="public">
	    <cfargument name="cascadeAbort" required="false" default="false" hint="cascades the abort to all pagelets from here on, not just in this space">
		<cfset variables.iCurrentPos = arrayLen(variables.arrSpacePagelets)>
	    <cfif arguments.cascadeAbort><cfset variables.aborted = true></cfif>
	</cffunction>

	<!--- Page is also used to return components to controller layers --->
	<cffunction name="getComponent" access="public">
		<cfargument name="sType" type="string" required="yes">
		<cfargument name="sExtension" type="string" required="yes">
		<cfargument name="sPath" type="string" required="yes">
		<cfif lcase(sType) EQ "extension">
			<cfscript>compTemp = createObject("component", "extensions.#sExtension#.#sPath#");</cfscript>
		<cfelse>
			<cfscript>compTemp = createObject("component", "#variables.rhino.getName()#.#sPath#");</cfscript>
		</cfif>
		<cfreturn compTemp>
	</cffunction>


	<!--- provide the details of the root file for rhino --->
	<cffunction name="getRhinoIndexFile" access="public" output="false">
		<cfreturn variables.rhino.getRhinoIndexFile()>
	</cffunction>

</cfcomponent>
