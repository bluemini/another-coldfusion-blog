<cfcomponent output="false">
    
    <cfscript>
        instance = StructNew();
        instance.RHINO_SPACE_CACHESAVE = 5;
        instance.RHINO_SPACE_CACHECHECK = 4;
        instance.SPACE_BEGIN_BIT = 3;
        instance.SPACE_COMPLETE_BIT = 2;
        instance.PAGELET_BEGIN_BIT = 1;
        instance.PAGELET_COMPLETE_BIT = 0;
    </cfscript>

	<cffunction name="init" access="public" output="false">
	    <cfargument name="refRhino" type="rhino" required="true">
	    <cfargument name="refPage" type="page" required="true">
	    <cfargument name="refAsset" type="asset" required="true">

	    <cfset instance.rhino = arguments.refRhino>
        <cfset instance.page = arguments.refPage>
        <cfset instance.asset = arguments.refAsset>

	    <cfreturn this>
	</cffunction>

	<cffunction name="getSpace" access="public" returntype="string" output="false">
		<cfargument name="spaceName" type="string" required="true">
		<cfargument name="type" type="string" required="false" default="content">
        <cfargument name="eventBitMap" type="numeric" required="false" default="0" hint="bitmap (4 bits) for space_begin, space_complete, pagelet_begin, pagelet_complete">

		<cfset var sOutput = "">
        <cfset var pageletOutput = "">
        <cfset var startTick = 0>
        <cfset var io = StructNew()>
        
		<!--- initialise the space within the page cfc --->
		<cfif instance.page.setSpaceName(arguments.spaceName)>
		
            <!--- fire an on space begin event --->
            <cfif BitMaskRead(arguments.eventBitMap, instance.RHINO_SPACE_CACHECHECK, 1)>
                <cfset eventStruct = StructNew()>
                <cfset eventStruct.sEvent = "RHINO_SPACE_CACHECHECK">
                <cfset eventStruct.spaceName = spaceName>
                <cfset eventStruct.spaceType = type>
                <cfset io = instance.rhino.getEvent().callHandler(argumentCollection=eventStruct)>
            </cfif>
        
            <cfsetting enablecfoutputonly="true">
        
            <cfif StructKeyExists(io, "data") AND io.data IS NOT "">
                <cfset sOutput = "<!-- cached data --><div style='background-color:##FFF0F0'>#io.data#</div><!-- end cached data -->">
            <cfelse>
        
                <!--- loop over all the pagelets in this space, page.cfc will return false when all pagelets have been requested --->
                <cfloop condition="instance.page.getNextSpacePagelet()">
                
                    <!--- fetch the name and type for the pagelet being requested --->
                    <cfset sPageletName = instance.page.getNextSpacePageletName()>
                    <cfset sPageletType = instance.page.getNextSpacePageletType()>
                
                    <!--- Execute pagelet --->
                    <cfset startTick = getTickCount()>
                    <cfset sOutput = sOutput & instance.asset.getAsset(sPageletName, sPageletType)>
                
                    <!--- show each pagelet inside a div and apply the name of the pagelet --->
                    <cfif StructKeyExists(url, "showtimes")>
                        <cfset sOutput = "<div style=""float:right"">#getTickCount()-startTick#ms#sOutput#</div>">
                    </cfif>
                
                    <!--- if showing pagelets, then close the div --->
                    <cfif structKeyExists(url, "showpagelets")>
                        <cfset sOutput = "<div style=""clear:both""></div><div style=""border: 1px solid ##FF6666;""><div style=""float: right; background-color: ##99FF99; border: 1px solid ##00FF00; color: ##666666; font-family: verdana,sans-serif; font-size: x-small; display: block;"">#sPageletName#</div>#sOutput#</div>">
                    </cfif>
            
                </cfloop><!--- loop over all the pagelets in a space --->
            
                <!--- show each space inside a div and apply the name of the space --->
                <cfif structKeyExists(url, "showspaces")>
                    <cfset sOutput = "<div style=""border: 1px solid blue;""><div style=""clear:both""></div><div style=""float: right; background-color: ##FFFF99; border: 1px solid ##FFFF00; color: ##666666; font-family: verdana,sans-serif; font-size: x-small; display: block;"">#arguments.spaceName#</div>#sOutput#</div>">
                </cfif>
        
                <!--- cache the response, if asked --->
                <cfif BitMaskRead(arguments.eventBitMap, instance.RHINO_SPACE_CACHESAVE, 1)>
                    <cfset eventStruct = StructNew()>
                    <cfset eventStruct.sEvent = "RHINO_SPACE_CACHESAVE">
                    <cfset eventStruct.spaceName = spaceName>
                    <cfset eventStruct.spaceType = type>
                    <cfset eventStruct.output = sOutput>
                    <cfset instance.rhino.getEvent().callHandler(argumentCollection=eventStruct)>
                </cfif>

            </cfif>

            <cfsetting enablecfoutputonly="false">

		</cfif> <!--- if the spacename returns ok --->
		<cfreturn Trim(sOutput)>
	</cffunction>

</cfcomponent>
