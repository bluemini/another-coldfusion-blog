<cfcomponent hint="the abstract class for all defining classes">
    
    <!---*** applet provides the basis of all application and extension root classes. An
    extension XYZ MUST have a component XYZ.cfc in it's root folder. This component should
    extend this class so that the basic methods provided here are available to the rest of 
    the framework. --->
	
    <!--- when an extension intercepts a request, it must pass back valid data for a variety
    of rendering requirements. This function sets the empty data structures --->
    <cffunction name="init" output="false">
    	<cfset instance = structNew()>
        <cfset instance.config = structNew()>
        <cfset instance.config.pages = structNew()>
        <cfset instance.config.pagelets = structNew()>
        <cfset instance.config.locals = structNew()>
        <cfset instance.config.datasets = structNew()>
        <cfset instance.config.controllers = structNew()>
        <cfset instance.scope = "application">
    </cffunction>

    <!--- When a user requests a page that is not available in the root applications pages.xml
    file, the framework fires this event against the extension named in the first part of the
    layout requested. If the extension knows how to respond to the request it sends back a valid
    page construction or controller object. --->
    <cffunction name="onRequestPageUndefined" access="public" output="false">
		<cfif structKeyExists(instance.config.pages, request.layout)>
            <cfset instance.config.pages[request.layout].scope = getScope()>
            <cfset instance.config.pages[request.layout].handler = "page">
            <cfreturn instance.config.pages[request.layout]>
        <cfelseif structKeyExists(instance.config.controllers, request.layout)>
            <cfset strReturn = structNew()>
            <cfset strReturn.controller = instance.config.controllers[request.layout]>
            <cfset strReturn.scope = getScope()>
            <cfset strReturn.handler = "controller">
            <cfreturn strReturn>
        </cfif>
    </cffunction>
    
    <!--- This function is deprecated, however, it is supported here for backwards compatability --->
    <cffunction name="onRequestPageletUndefined" access="public" output="false">
        <cfreturn this.onRequestPageUndefined()>
    </cffunction>
	
	<cffunction name="setConfig" access="public" output="false">
		<cfargument name="strConfiguration" type="struct">
		<cfset instance.config = arguments.strConfiguration>
	</cffunction>
    
    <cffunction name="setScope" access="public" output="false">
        <cfargument name="sScope" type="string" required="true">
        <cfset instance.scope = arguments.sScope>
    </cffunction>
    
    <cffunction name="getScope" access="public">
        <cfreturn instance.scope>
    </cffunction>
    
    <cffunction name="getPages" access="public" output="false">
    </cffunction>
    
    <cffunction name="getLocal" access="public" output="false">
        <cfargument name="sLocal" type="string" required="true">
        <cfif structKeyExists(instance.config.locals, sLocal)>
            <cfreturn instance.config.locals[sLocal]>
        </cfif>
        <cfreturn "">
    </cffunction>
    
	<!--- an 'abstract' method that returns a default response if not overridden --->
	<cffunction name="sesParser" access="public" output="false" returntype="struct">
		<cfset var response = {}>
		<cfset response.success = false>
		<cfreturn response>
	</cffunction>
	
</cfcomponent>