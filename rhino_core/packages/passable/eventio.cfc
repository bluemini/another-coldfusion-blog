<cfcomponent>

	<cfset this.instance = structNew()>
    <cfset this.clear()>
	
	<cffunction name="clear" access="public">
		<cfset structClear(this.instance)>
	    <cfset this.instance.set = false>
	    <cfset this.instance.data = "">
	    <cfset this.instance.handledBy ="">
	</cffunction>
    
    <cffunction name="set" access="public">
        <cfargument name="value" type="any">
        <cfargument name="sHandledBy" type="string" required="false" default="">
        <cfset this.instance.data = value>
        <cfset this.instance.handledBy = sHandledBy>
        <cfset this.instance.set = true>
    </cffunction>
    
    <cffunction name="isSet" access="public" returntype="boolean">
        <cfreturn this.instance.set>
    </cffunction>
    
    <cffunction name="getData" access="public" returntype="any">
        <cfif this.instance.set><cfreturn this.instance.data>
            <cfelse><cfreturn ""></cfif>
    </cffunction>

    <cffunction name="getHandler" access="public" returntype="any">
        <cfreturn this.instance.handledBy>
    </cffunction>

</cfcomponent>