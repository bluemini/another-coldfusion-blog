<cfcomponent output="false">
	
	<cfset variables.IBOCurrentRecord = 1>

	<cffunction name="wireup" access="public" output="false">
		<cfargument name="wireBeans">
		
		<cfset var wiredBeans = structKeyArray(this.dependents)>
		<cfset var iBean = 0>
		<cfset compAlias = "">
		
		<cfset variables.beans = structNew()>
		
		<cfloop from="1" to="#arrayLen(wiredBeans)#" index="iBean">
			<cfset compAlias = wiredBeans[iBean]>
			<cfif structKeyExists(arguments.wireBeans, this.dependents[compAlias].class)>
				<cfset variables.beans[compAlias] = arguments.wireBeans[this.dependents[compAlias].class]>
			</cfif>
		</cfloop>
	</cffunction>
	
	<cffunction name="wireDependent" access="public" output="false">
		<cfargument name="componentAlias" type="string" required="true">
		<cfargument name="componentScope" type="string" required="false" default="application">
        <cfargument name="componentLocation" type="string" required="false" default="">
		
		<cfset var wiredBean = request.rhino.getWire(arguments.componentAlias)>
		
		<cfif NOT structKeyExists(this, "dependents")><cfset this.dependents = structNew()></cfif>
		
		<cfif wiredBean IS "">
            <cfif arguments.componentLocation IS NOT "">
                <cfset wiredBean = arguments.componentLocation>
            <cfelse>
                <cfthrow message="Auto-wired bean is not available."
                    detail="You have attempted to wire a component with an alias '#arguments.componentAlias#', however, no component with that alias can be found.">
            </cfif>
		</cfif>
		
		<cfset this.dependents[arguments.componentAlias] = structNew()>
		<cfset this.dependents[arguments.componentAlias].class = wiredBean>
		<cfset this.dependents[arguments.componentAlias].scope = arguments.componentScope>
	</cffunction>

	<cffunction name="setAttributes" access="public" output="false">
		<cfargument name="attributes" type="struct">
		<cfset variables.attributes = arguments.attributes>
		<cfset this.attributes = arguments.attributes>
	</cffunction>


	<!--- add some iterators for multi-value query-backed objects --->
	<cffunction name="IBOSetupIterator" access="public" output="false">
		<cfargument name="data" hint="the Query to back the iterating object">
		<cfargument name="dataName" hint="the key of the data in the 'variables' scope used by generall accessors">
		<cfargument name="dataStart" required="false" type="numeric" default="0" hint="the start record">
		
		<cfif NOT isQuery(arguments.data)>
			<cfthrow message="Unable to setup iterator. Data '#arguments.dataName#' is not a Query.">
		</cfif>

		<!--- <cfif arguments.data.recordCount EQ 0>
			<cfthrow message="Unable to setup iterator. Data '#arguments.dataName#' has no records.">
		</cfif> --->

		<cfif arguments.data.recordCount LT arguments.dataStart OR arguments.dataStart LT 0>
			<cfthrow message="Unable to setup iterator. Data '#arguments.dataStart#' is either less than 1 or greater than the available records.">
		</cfif>
		
		<cfset variables.IBOCurrentRecord = arguments.dataStart>
		<cfset variables.IBODataCollection = arguments.data>
		<cfset variables.IBODataName = arguments.dataName>
		<cfset variables[variables.IBODataName] = variables.IBODataCollection>
		<cfset variables.IBOColumnList = variables.IBODataCollection.columnList>
		
		<!--- <cfset this.IBOSetRecord()> --->
	</cffunction>
	
	<cffunction name="IBOGetNext" access="public" output="false">
		<cfif variables.IBOCurrentRecord LT variables.IBODataCollection.recordCount>
			<cfset variables.IBOCurrentRecord = variables.IBOCurrentRecord + 1>
			<!--- <cfset this.IBOSetRecord()> --->
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>
	
	<cffunction name="IBOSetRecord" access="public" output="false">
		<!--- <cfset var columnName = "">
		<cfset var newQuery = queryNew(variables.IBOColumnList)>
		<cfset queryAddRow(newQuery)>
		<cfloop list="#variables.IBOColumnList#" index="columnName">
			<cfset querySetCell(newQuery, columnName, variables.IBODataCollection[columnName][variables.IBOCurrentRecord])>
		</cfloop>
		<cfset variables[variables.IBODataName] = newQuery> --->
	</cffunction>
	
	<cffunction name="IBOGetCurrentRow" access="public" output="false">
		<cfreturn variables.IBOCurrentRecord>
	</cffunction>
	<cffunction name="IBOGetTotalRows" access="public" output="false">
		<cfreturn variables.IBODataCollection.recordCount>
	</cffunction>
	<cffunction name="IBOMoveBeforeFirst" access="public" output="false">
		<cfset variables.IBOCurrentRecord = 0>
	</cffunction>
	<cffunction name="IBOMoveAfterLast" access="public" output="false">
		<cfset variables.IBOCurrentRecord = variables.IBODataCollection.recordCount + 1>
	</cffunction>
	
	<!--- return a boolean on whether the currentrow pointer is on the first record --->
	<cffunction name="IBOIsFirst" access="public" output="false">
		<cfif variables.IBOCurrentRecord EQ 1><cfreturn true><cfelse><cfreturn false></cfif>
	</cffunction>
	<!--- return a boolean on whether the currentrow pointer is on the last record --->
	<cffunction name="IBOIsLast" access="public" output="false">
		<cfif variables.IBOCurrentRecord EQ variables.IBODataCollection.recordCount><cfreturn true><cfelse><cfreturn false></cfif>
	</cffunction>

	<!--- provide access to the underlying query --->
	<cffunction name="getIBODataCollection" access="public" output="false">
		<cfreturn variables.IBODataCollection>
	</cffunction>

</cfcomponent>