<cfcomponent>

	<cfset params.error = arrayNew(1)>

	<!--- Primary method to see if any errors have been raised --->
	<cffunction name="hasErrors" access="public" returntype="boolean">
		<cfif arrayLen(params.error) GT 0><cfreturn true><cfelse><cfreturn false></cfif>
	</cffunction>

	<cffunction name="getErrors" access="public" returntype="array">
		<cfargument name="fMaintainErrorStack" type="boolean" required="no" default="false">
		<cfset arrTemp = params.error>
		<cfif NOT fMaintainErrorStack><cfset params.error = arrayNew(1)></cfif>
		<cfreturn arrTemp>
	</cffunction>
	
	<cffunction name="setError" access="public" returntype="boolean">
		<cfargument name="strError" type="struct" required="yes">
		<cftry>
			<cfset arrayAppend(params.error, arguments.strError)>
			<cfcatch><cfreturn false></cfcatch>
		</cftry>
		<cfreturn true>
	</cffunction>

</cfcomponent>