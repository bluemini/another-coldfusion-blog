<cfsilent>

	<cfinclude template="/rhino_core/rhino.cfm">

	<!--- check for and set some defaults --->
    <cfparam name="url.blogid" type="numeric">
    
    <!--- create any objects that we need --->
    <cfset objBlog = request.data.getObject("blog.obj_blog")>
	<cfset objPost = request.data.getObject("blog.obj_post")>
    
    <cfset qryBlog = objBlog.getBlog(url.blogid)>
    
    <cfset qryRecent = objPost.getLatestPosts(blogid=url.blogid, getFullData=true)>
    
    <cfsetting showdebugoutput="false">

</cfsilent><cfcontent reset="true" type="text/xml"><?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/" version="2.0">
  <channel>
    <cfoutput><title>#qryBlog.blog_name#</title>
    <link>http://#cgi.server_name#/blog/#qryBlog.blog_name_user#/</link>
    <description>#XMLFormat(qryBlog.blog_about_text)#</description></cfoutput>
<cfoutput query="qryRecent">    <item>
      <title>#xmlFormat(post_title)#</title>
      <link>http://#cgi.server_name#/blog/#qryBlog.blog_name_user#/#post_slug#</link>
      <description>#xmlFormat(post_content)#</description>
      <pubDate>#dateFormat(post_date, "Ddd, DD Mmm YYYY")# #timeFormat(post_date, "HH:MM:SS")# GMT</pubDate>
    </item>
</cfoutput>  </channel>
</rss>