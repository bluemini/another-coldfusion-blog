<cfif StructKeyExists(cookie, request.AuthId)>
	<cfcookie name="acfbid" value="#cookie[request.AuthId]#">

    <cfif NOT StructKeyExists(application, "auth")>
    	<cfset application.auth = StructNew()>
    </cfif>

    <cfif StructKeyExists(application.auth, url.cfid)>
    	<cfset application.auth[url.cfid]++>
        <cfif application.auth[url.cfid] GT 0>
			Been here before!!<cfabort>
        	<cflocation url="forbidden.cfm">
        </cfif>
    <cfelse>
    	<cfset application.auth[url.cfid] = 0>
    </cfif>
<cfelse>
	No cookie...oops
	<cfdump var="#cookie#">
	<cfabort>
</cfif>

<cfheader name="location" value="#url.location#">
<cfheader statuscode="302" statustext="Moved temporarily">